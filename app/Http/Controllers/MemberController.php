<?php

namespace App\Http\Controllers;

use App\Models\Margin;
use App\Models\MarginDetail;
use App\Models\MasterPlu;
use App\Models\MasterToko;
use App\Models\Member;
use App\Models\UserTmi;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getMemberTmi(Request $request){
        ini_set('memory_limit', '-1');
        $memberFormat="";
        $tipeFormat="";
        $branchFormat="";
        $idcab = $request->get('id_cab');

        $countmember = \DB::connection('tmiivan')->table('users')->count();


        if($countmember > 0){
            $memberTmi = UserTmi::distinct()->select('member_code')->lists('member_code')->toArray();


            $MemberBinaan = \DB::table('customers')
                ->whereNotIn('kode_member', $memberTmi)
                ->Take(10)
                ->Get();

            foreach($MemberBinaan as $index => $row) {
                $memberFormat .= "<option style='font-size: 12px;' value='" . $row->kode_member . "'>" . $row->kode_member . " --> " . $row->name . "</option>";
            }

        }



        $tipeMember = \DB::table('tipe_tmi')
            ->Get();

        foreach($tipeMember as $index => $row) {
            $tipeFormat .= "<option style='font-size: 12px;' value='" . $row->kode_tmi . "'>" . $row->nama . "</option>";
        }

        $branch = \DB::table('branches')
            ->get();

        foreach($branch as $index => $row) {
            $branchFormat .= "<option style='font-size: 12px;' value='" . $row->kode_igr . "'>" . $row->kode_igr . " --> " . $row->name . "</option>";
        }






        return view('admin.inputmember')->with('member',$memberFormat)->with('tipetmi',$tipeFormat)->with('branch',$branchFormat);
    }

    public function getMemberDatatables(){
        $MemberTmiAssoc = UserTmi::SelectRaw('email, store_name, branches.branch_name as cabang, master_tmi_ids.description as tipetmi, member_code, users.id as idmember, flag_aktif, users.phone_number as phone_number, users.address as addressmember')
            ->leftJoin('master_tmi_ids', 'users.tmi_id', '=', 'master_tmi_ids.tmi_code')
            ->leftJoin('branches', 'users.branch_code', '=', 'branches.branch_code')
            ->get();


        foreach ($MemberTmiAssoc as $row) {
            if ($row->flag_aktif == 0) {
//                $row->aksi .= "<span>-</span>";
                $row->status .= "<span class='label label-danger flat' style='font-size: 15px; font-weight: bold;'>Member Belum Aktif</span>";
            }else{
                $row->status = '<button id="btn_edit_member" type="button" class="btn btn-info flat" style="width:70px;margin-bottom: 5px;" value="' . $row->idmember . '"> Edit </button>';
//                $row->status = "<span class='label label-info flat' style='font-size: 15px; font-weight: bold;'>Member Sudah Aktif</span>";
            }

        }
        return \Datatables::of($MemberTmiAssoc)->make(true);

    }

    public function get_member(Request $request){
        $html_cab = '<option value=0>-- Pilih Member --</option>';
        $idcab = $request->get('id_cab');

        $memberTmi = UserTmi::distinct()->select('member_code')->lists('member_code')->toArray();

        $MemberBinaan = \DB::table('customers')
            ->whereNotIn('kode_member', $memberTmi)
            ->where('kode_igr', $idcab)
//            ->Take(10)
            ->Get();

        foreach ($MemberBinaan as $key => $row) {
            $html_cab .= "<option style='font-size: 12px;' value='" . $row->kode_member . "'>" . $row->kode_member . " --> " . $row->name . "</option>";
        }
        return $html_cab;
    }

    public function getRegisterMember(Request $request){

        $getnama = \DB::table('customers')
//            ->Select('name')
            ->where('kode_member', $request->member)
            ->Pluck('name');

        \DB::beginTransaction();
        try{
            $igr_code = $request->txt_cab;

            if($igr_code == '01'){
                $con = 'igrcpg';
            }elseif($igr_code == '03'){
                $con = 'igrsby';
            }elseif($igr_code == '04'){
                $con = 'igrbdg';
            }elseif($igr_code == '05'){
                $con = 'igrtgr';
            }elseif($igr_code == '06'){
                $con = 'igrygy';
            }elseif($igr_code == '15'){
                $con = 'igrmdn';
            }elseif($igr_code == '16'){
                $con = 'igrbks';
            }elseif($igr_code == '17'){
                $con = 'igrplg';
            }elseif($igr_code == '18'){
                $con = 'igrkmy';
            }elseif($igr_code == '20'){
                $con = 'igrpku';
            }elseif($igr_code == '21'){
                $con = 'igrsmd';
            }elseif($igr_code == '22'){
                $con = 'igrsmg';
            }elseif($igr_code == '25'){
                $con = 'igrbgr';
            }elseif($igr_code == '26'){
                $con = 'igrptk';
            }elseif($igr_code == '27'){
                $con = 'igrbms';
            }elseif($igr_code == '28'){
                $con = 'igrmdo';
            }elseif($igr_code == '31'){
                $con = 'igrmks';
            }elseif($igr_code == '32'){
                $con = 'igrjbi';
            }elseif($igr_code == '33'){
                $con = 'igrkri';
            }elseif($igr_code == '35'){
                $con = 'igrcpt';
            }elseif($igr_code == '36') {
                $con = 'igrkrw';
            }else{
                return null;
            }

            $response = array();

            $response["email"] = $request->email;
            $response["password"] = $request->password;
            $response["user_name"] = $getnama;
            $response["store_name"] = $request->namatoko;
            $response["branch_code"] = $request->txt_cab;
            $response["member_code"] = $request->member;
            $response["flag_aktif"] = 0;
            $response["tmi_id"] = $request->txt_tipe;
            $response["phone_number"] = $request->nohape;
            $response["address"] = $request->alamat;
            $response["created_at"] = Carbon::now();
            $response["updated_at"] = Carbon::now();
            $response["created_by"] = 'ADM';
            $response["total_product"] = 0;

            $ch = curl_init('http://mitra.indogrosir.co.id/doRegisterAPI');
//            curl_setopt($ch, CURLOPT_URL, $ch);
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'X-Authorization: d22cc9cf7ba5462da85d62c8b856e185e112c361'
            ));
            $array = ['user_name'=>$getnama, 'email'=>$request->email, 'store_name'=>$request->namatoko, 'password'=>$request->password, 'phone_number'=>$request->nohape,
                'member_code'=>$request->member, 'branch_code'=>$request->txt_cab, 'address'=>$request->alamat, 'flag_aktif'=>0, 'tmi_id'=>$request->txt_tipe, 'total_product'=>0,
                'created_by'=>'ADM'];

            curl_setopt($ch, CURLOPT_POSTFIELDS, $array);

            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

            //execute post
            $result = curl_exec($ch);

            //close connection
            curl_close($ch);

            \DB::connection($con)->table('tbmaster_customer')
                ->where('cus_kodemember', $request->member)
                ->Where('cus_kodeigr', $request->txt_cab)
                ->update(['cus_jenismember' => 'T']);

            \DB::commit();

            return 'TRUE';

        }catch(Exception $ex){
            \DB::rollBack();
            return redirect('register')->with('err', 'Gagal menyimpan data, silahkan coba lagi');
        }

    }


    public function EditMember(Request $request){
        $id = $request->id;

        $data = UserTmi::find($id);
        $data->tmi_id = $request->tipetmi;
        $data->store_name = $request->namatoko;
        $data->phone_number = $request->nohp;
        $data->address = $request->address;
        $data->save();

        return "true";
    }


    public function getMemberAjax(Request $request){
        $id = $request->id;

        $data = UserTmi::distinct()
            ->SelectRaw('email, store_name, branches.branch_name as cabang, master_tmi_ids.description as tipetmi, member_code, users.id as idmember, flag_aktif, users.phone_number as phone_number, users.address as addressmember')
//            ->leftJoin('tipe_tmi', 'users.tmi_id', '=', 'tipe_tmi.kode_tmi')
            ->leftJoin('master_tmi_ids', 'users.tmi_id', '=', 'master_tmi_ids.tmi_code')
            ->leftJoin('branches', 'users.branch_code', '=', 'branches.branch_code')
            ->Where('users.id', $id)
            ->get();

        return $data;
    }



}
