<?php

namespace App\Http\Controllers;

use App\Models\Margin;
use App\Models\MarginDetail;
use App\Models\MasterPlu;
use App\Models\MasterToko;
use App\Models\Member;
use App\Models\TDBBranch;
use App\Models\TDBOperator;
use App\Models\TDBPbDetail;
use App\Models\TDBPbHeader;
use App\Models\TDBTrxHeader;
use App\Models\TDBUser;
use App\Models\TDBUserProduct;
use App\Models\UserTmi;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.inputmember');
    }


    public function getViewMember()
    {
        return view('admin.uploadplu')->with('getmember', $this->MemberView());
    }

    public function getUploadMargin()
    {

        return view('admin.uploadmargin');

    }

    public function getDepartemen(Request $Request){
        $DIV = $Request -> get('div');
        $Departemen = \DB::Table('department')->Distinct()
            ->Select('dep_kodedivisi', 'dep_namadepartement', 'dep_kodedepartement')
            ->Where('DEP_KODEDIVISI', "LIKE", "%" . $DIV . "%")
            ->OrderBy('DEP_KODEDEPARTEMENT')
            ->Get();
        $depFormat="<option>--Pilih Departemen--</option>";

        foreach($Departemen as $index => $row) {
            $depFormat .= "<option style='font-size: 12px;' value='" . $row->dep_kodedepartement . "'>(" . $row->dep_kodedepartement . ") " . $row->dep_namadepartement . "</option>";
        }
        return $depFormat;
    }

    public function getKategori(Request $Request){
        $DEP = $Request -> get('dep');
        $Kategori = \DB::Table('category')->Distinct()
            ->Select('kat_kodedepartement', 'kat_namakategori', 'kat_kodekategori')
            ->Where('kat_kodedepartement',  "LIKE", "%" . $DEP . "%")
            ->OrderBy('KAT_KODEKATEGORI')
            ->Get();

        $katFormat="<option>--Pilih Kategori--</option>";

        foreach($Kategori as $index => $row) {
            $katFormat .= "<option style='font-size: 12px;' value='" . $row->kat_kodekategori . "'>(" . $row->kat_kodekategori . ") " . $row->kat_namakategori . "</option>";
        }
        return $katFormat;
    }

    public function getDashBoard()
    {

//        $Member = \DB::table('master_plu')
//            ->SelectRaw('nama, count(kodeplu)as sumplu, tipe_tmi.id as idtoko')
//            ->leftJoin('tipe_tmi', 'tipe_tmi.id', '=', 'master_plu.tmi_id')
//            ->GroupBy('nama')
//            ->Get();

        $countaktif = \DB::table('master_toko')
            ->wherenotnull('status')
            ->count();

        $countbelumaktif = \DB::table('master_toko')
            ->whereNull('status')
            ->count();


        return view('admin.404')->with('aktif', $countaktif)->with('belumaktif', $countbelumaktif);
    }


    public function getListMasterMargin()
    {
        $memberFormat="";
        $pluFormat="";
        $branchFormat="";
        $tipeFormat="";
        $prodmast ="";


        $divFormat="";
        $depFormat="";
        $katFormat="";

        $divisiAssoc = \DB::Table('divisi')->Distinct()
            ->Select('div_namadivisi', 'div_kodedivisi')
            ->OrderBy('DIV_KODEDIVISI')
            ->Get();

        $departemenAssoc = \DB::Table('department')->Distinct()
            ->Select('dep_kodedivisi', 'dep_namadepartement', 'dep_kodedepartement')
//            ->Where('DEP_KODEDIVISI', "LIKE", "%" . $divisi . "%")
            ->OrderBy('DEP_KODEDEPARTEMENT')
            ->Get();

//        dd($departemenAssoc);

        $kategoriAssoc = \DB::Table('category')->Distinct()
            ->Select('kat_kodedepartement', 'kat_namakategori', 'kat_kodekategori')
//            ->Where('KAT_KODEDEPARTEMEN',  "LIKE", "%" . $departemen . "%")
            ->OrderBy('KAT_KODEKATEGORI')
            ->Get();


        foreach($divisiAssoc as $index=> $row) {

            $divFormat .= "<option style='font-size: 12px;' value='". $row->div_kodedivisi ."'>(" . $row->div_kodedivisi . ") " . $row->div_namadivisi . "</option>";
        }
        foreach($departemenAssoc as $index => $row) {
            $depFormat .= "<option style='font-size: 12px;' value='" . $row->dep_kodedepartement . "'>(" . $row->dep_kodedepartement . ") " . $row->dep_namadepartement . "</option>";
        }
        foreach($kategoriAssoc as $index => $row) {
            $katFormat .= "<option style='font-size: 12px;' value='" . $row->kat_kodekategori . "'>(" . $row->kat_kodekategori . ") " . $row->kat_namakategori . "</option>";
        }


        $branch = \DB::table('branches')
            ->get();

        $tipeMember = \DB::table('tipe_tmi')
            ->Get();

        foreach($tipeMember as $index => $row) {
            $tipeFormat .= "<option style='font-size: 12px;' value='" . $row->kode_tmi . "'>" . $row->nama . "</option>";
        }

        foreach($branch as $index => $row) {
            $branchFormat .= "<option style='font-size: 12px;' value='" . $row->kode_igr . "'>" . $row->kode_igr . " --> " . $row->name . "</option>";
        }


        return view('admin.listmargin')->with('member',$memberFormat)->with('plu',$pluFormat)->with('prodmast',$prodmast)->with('branch',$branchFormat)->with('tipetmi',$tipeFormat)->with('divisiOpt', $divFormat)->with('departemenOpt', $depFormat)->with('kategoriOpt', $katFormat);
    }

    function incrementLetter($val, $increment = 2)
    {
        for ($i = 1; $i <= $increment; $i++) {
            $val++;
        }

        return $val;
    }

    public function getListMember()
    {
        $memberFormat="";
        $pluFormat="";
        $branchFormat="";
        $tipeFormat="";
        $prodmast ="";


        $MemberTMI = \DB::table('tipe_tmi')
            ->get();

        $branch = \DB::table('branches')
            ->get();

        $tipeMember = \DB::table('tipe_tmi')
            ->Get();

        foreach($tipeMember as $index => $row) {
            $tipeFormat .= "<option style='font-size: 12px;' value='" . $row->kode_tmi . "'>" . $row->nama . "</option>";
        }

        foreach($branch as $index => $row) {
            $branchFormat .= "<option style='font-size: 12px;' value='" . $row->kode_igr . "'>" . $row->kode_igr . " --> " . $row->name . "</option>";
        }

        foreach($MemberTMI as $index => $row) {
            $memberFormat .= "<option style='font-size: 12px;' value='" . $row->id . "'>" . $row->id . " --> " . $row->nama . "</option>";
        }



        return view('admin.listmember')->with('member',$memberFormat)->with('plu',$pluFormat)->with('prodmast',$prodmast)->with('branch',$branchFormat)->with('tipetmi',$tipeFormat);
    }


    public function getCabangViewAjax(Request $Request){

        if($Request->ajax()){
            $cFormatRaw="";

            $id = $Request->id;

            $CabAssoc = Margin::SelectRaw('name, nama, kat_namakategori')
                ->leftJoin('margin_details', 'master_margin.id', '=', 'margin_details.margin_id')
                ->leftJoin('tipe_tmi', 'master_margin.kode_tmi', '=', 'tipe_tmi.kode_tmi')
                ->leftJoin('branches', 'margin_details.kode_igr', '=', 'branches.kode_igr')
                ->leftJoin('divisi', 'div_kodedivisi', '=', 'div')
//            ->Join('department', 'dep_kodedepartement', '=', 'dep')
                ->leftJoin('department', function ($join) {
                    $join->on('master_margin.dep', '=', 'department.dep_kodedepartement');
                    $join->on('divisi.div_kodedivisi', '=', 'department.dep_kodedivisi');
                })
                ->leftJoin('category', function ($join) {
                    $join->on('master_margin.kat', '=', 'category.kat_kodekategori');
                    $join->on('department.dep_kodedepartement', '=', 'category.kat_kodedepartement');
                })
                ->Where('master_margin.id', $id)
                ->Where('flag_cab', 0)
                ->WhereNull('deleted_at')
//            ->Join('category', 'kat_kodekategori', '=', 'kat')
                ->get();

//            dd($data);

            $cFormatRaw = "<div class='table-responsive'>
                            <table class='table'>
                                        <tr style='text-align: center'>
                                                <th class='font-12' style='text-align: center;'>
                                                    Nama Cabang
                                                </th>
                                                <th class='font-12' style='text-align: center;'>
                                                    Tipe TMI
                                                </th>
                                                <th class='font-12' style='text-align: center;'>
                                                    Kategori
                                                </th>
                                            </tr>";

            $sindex = 0;

            foreach ($CabAssoc as $row){

                $cFormatRaw .= "
                                    <tr>
                                     <td class=\"font-12\"; style='vertical-align: middle; text-align: center;'>
                                             " . $row->name . "
                                        </td>
                                        <td class=\"font-12\"; style='vertical-align: middle; text-align: center;'>
                                             " . $row->nama . "
                                        </td>
                                         <td class=\"font-12\"; style='vertical-align: middle; text-align: center;'>
                                               " . $row->kat_namakategori . "
                                        </td>


                                    </tr>";
            }
            $cFormatRaw .= "</table>";
            $cFormatRaw .= "</div>";
            return $cFormatRaw;
        }
    }

    public function getMarginDatatable(){
        $MarginAssoc = Margin::SelectRaw('tipe_tmi.nama as tipetmi, margin_min, margin_max, margin_saran, kat_namakategori, master_margin.id as idmrg, flag_cab')
            ->leftJoin('tipe_tmi', 'master_margin.kode_tmi', '=', 'tipe_tmi.kode_tmi')
//            ->leftJoin('branches', 'master_margin.kode_igr', '=', 'branches.kode_igr')
            ->leftJoin('divisi', 'div_kodedivisi', '=', 'div')
//            ->Join('department', 'dep_kodedepartement', '=', 'dep')
            ->leftJoin('department', function ($join) {
                $join->on('master_margin.dep', '=', 'department.dep_kodedepartement');
                $join->on('divisi.div_kodedivisi', '=', 'department.dep_kodedivisi');
            })
            ->leftJoin('category', function ($join) {
                $join->on('master_margin.kat', '=', 'category.kat_kodekategori');
                $join->on('department.dep_kodedepartement', '=', 'category.kat_kodedepartement');
            })
            ->OrderBy('master_margin.id', 'DESC')
//            ->Join('category', 'kat_kodekategori', '=', 'kat')
            ->get();

        foreach ($MarginAssoc as $row) {
            if ($row->flag_cab == 1) {
                $row->cab = "<span class='label label-danger flat' style='font-size: 15px; font-weight: bold;'>All Cabang</span>";
            }else{
//                $row->cab = "<span class='label label-danger flat' style='font-size: 15px; font-weight: bold;'>Dicabang Tertentu</span>";
                $row->cab = '<button id="btn_cabang_ongkir" type="button" class="btn btn-info flat" style="width:100px;margin-bottom: 5px;" value="' . $row->idmrg . '"> Lihat Cabang </button>';
            }
            $row->aksi = '<button id="btn_edit_margin" type="button" class="btn btn-info flat" style="width:70px;margin-bottom: 5px;" value="' . $row->idmrg . '"> Edit </button>';
        }
        return \Datatables::of($MarginAssoc)->make(true);
    }

    public function getStoreOfBranch(Request $request)
    {
        $id = $request->get('branch');

        $stores = TDBUser::where('branch_id','=',$id)->get();
//        $stores = TDBUser::where(function($query) use($id){
//                                foreach($id as $item){
//                                    $query->orWhere('branch_id','LIKE',$item);
//                                }
//                            })->get();

        $optionstore = "<option style='font-size: 12px;' value='%'>SEMUA TOKO</option>";
        foreach ($stores as $store)
        {
            $optionstore.="<option style='font-size: 12px;' value='" . $store->id . "'>(" . $store->member_code . ") " . $store->store_name . "</option>";
        }

        return $optionstore;
    }

    public function getCashierOfStore(Request $request)
    {
        $id = $request->get('store');

        $operators = TDBOperator::where('user_id', '=', $id)->get();

        $optionoperator = "<option style='font-size: 12px;' value='%'>SEMUA KASIR</option>";
        foreach ($operators as $operator) {
            $optionoperator .= "<option style='font-size: 12px;' value='" . $operator->id . "'>(" . $operator->code . ") " . $operator->name . "</option>";
        }

        return $optionoperator;
    }


    public function getSalesChart()
    {
        $data = TDBBranch::with('users','users.trxHeaders')
                ->get();

        $datasets = array();
        foreach($data as $branch)
        {
            $branchname = $branch->name;
            $branchid = $branch->id;
            $total = 0;
            foreach($branch->users as $user)
            {
                foreach($user->trxHeaders as $header)
                {
                    $total += $header->grand_total;
                }
            }
            $datasets[] = ['branchname' => $branchname, 'total' => $total, 'id' => $branchid];
        }

        return json_encode($datasets);
    }

    public function returnSales(Request $request)
    {
        $id = $request->get('branchid');
        return view('admin.laporansales')->with('optionbranch',$this->getSelectedBranches($id))->with('tahun', $this->getYearOfTrxHeader());
    }

    public function returnPareto()
    {
        return view('admin.laporanpareto')->with('optionbranch',$this->getBranches())->with('tahun', $this->getYearOfTrxHeader());
    }

    public function returnPb()
    {
        return view('admin.laporanpb')->with('optionbranch',$this->getBranches())->with('tahun', $this->getYearOfPbHeader());
    }

    public function returnPromo()
    {
        return view('admin.laporanpromosi')->with('optionbranch',$this->getBranches());
    }

    public function returnArchive()
    {
        return view('admin.arsipproduk')->with('optionbranch',$this->getBranches());
    }

    public function returnStock()
    {
        return view('admin.stokproduk')->with('optionbranch',$this->getBranches());
    }

    public function getYearOfTrxHeader()
    {
        $result = TDBTrxHeader::select(\DB::raw('YEAR(trx_date) as year'))->distinct()->get();
        $years = $result->pluck('year');
        $optionyear = "";

        foreach ($years as $year)
        {
            $optionyear.="<option style='font-size: 12px;' value='" . $year . "-'>" . $year . "</option>";
        }

        return $optionyear;
    }

    public function getYearOfPbHeader()
    {
        $result = TDBPbHeader::select(\DB::raw('YEAR(po_date) as year'))->distinct()->get();
        $years = $result->pluck('year');
        $optionyear = "";

        foreach ($years as $year)
        {
            $optionyear.="<option style='font-size: 12px;' value='" . $year . "-'>" . $year . "</option>";
        }

        return $optionyear;
    }

    public function getSelectedBranches($id)
    {
        $branches = TDBBranch::all();

        $optionbranch = "<option style='font-size: 12px;' value='%'>SEMUA CABANG</option>";
        foreach ($branches as $branch)
        {
            if($branch->id == $id)
            {
                $optionbranch.="<option style='font-size: 12px;' value='" . $branch->id . "' selected>(" . $branch->code . ") " . $branch->name . "</option>";
            }
            else
            {
                $optionbranch.="<option style='font-size: 12px;' value='" . $branch->id . "'>(" . $branch->code . ") " . $branch->name . "</option>";
            }
        }

        return $optionbranch;
    }

    public function getBranches()
    {
        $branches = TDBBranch::all();

        $optionbranch = "<option style='font-size: 12px;' value='%' selected>SEMUA CABANG</option>";
        foreach ($branches as $branch)
        {
            $optionbranch.="<option style='font-size: 12px;' value='" . $branch->id . "'>(" . $branch->code . ") " . $branch->name . "</option>";
        }

        return $optionbranch;
    }

    public function getSalesDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $cashier = $request->get('cashier');
        $date = $request->get('date');

        if($date === "daterange")
        {
            $startdate = $request->get('startdate')." 00:00:00";
            $enddate = $request->get('enddate')." 23:59:59";

            $data = TDBBranch::ConnectToHeader($branch,$store,$cashier)
                ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                ->get(['branches.name as branch','users.store_name as store','operators.name as cashier','trx_headers.trx_no as invoice','trx_headers.trx_date as date','trx_headers.grand_total as total','trx_headers.margin as margin']);
        }
        else
        {
            $data = TDBBranch::ConnectToHeader($branch,$store,$cashier)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->get(['branches.name as branch','users.store_name as store','operators.name as cashier','trx_headers.trx_no as invoice','trx_headers.trx_date as date','trx_headers.grand_total as total','trx_headers.margin as margin']);
        }

        $totalt = $data->sum('total');
        $totalm = $data->sum('margin');

        return \Datatables::of($data)->with('totalt',$totalt)->with('totalm',$totalm)->make(true);
    }

    public function getSalesDayDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $cashier = $request->get('cashier');
        $date = $request->get('date');

        $data = TDBBranch::ConnectToHeader($branch,$store,$cashier)
            ->where('trx_headers.trx_date','LIKE',$date)
            ->get(['branches.name as branch','users.store_name as store','trx_headers.trx_no as invoice','trx_headers.trx_date as date','trx_headers.grand_total as total','trx_headers.margin as margin']);

        $tabledata = [];
        $sales = [];
        $temp = 0;
        $temptotal = 0;

        $sales["header"] = "sales";
        for($i = 1; $i < 32; $i++)
        {
            $colname = "d".$i;
            foreach($data as $column)
            {
                if(date('d',strtotime($column->date)) == $i)
                {
                    $temp = $temp + $column->total;
                }
            }
            $sales[$colname] = $temp;
            $temptotal = $temptotal + $temp;
            $temp = 0;
        }
        $sales["total"] = $temptotal;
        $temptotal = 0;
        $numdays = cal_days_in_month(CAL_GREGORIAN, substr($date,5,2), substr($date,0,4));
        $sales["rata"] = round($sales["total"]/$numdays);
        array_push($tabledata, $sales);

        $sales["header"] = "margin (rupiah)";
        for($i = 1; $i < 32; $i++)
        {
            $colname = "d".$i;
            foreach($data as $column)
            {
                if(date('d',strtotime($column->date)) == $i)
                {
                    $temp = $temp + $column->margin;
                }
            }
            $sales[$colname] = $temp;
            $temptotal = $temptotal + $temp;
            $temp = 0;
        }
        $sales["total"] = $temptotal;
        $temptotal = 0;
        $numdays = cal_days_in_month(CAL_GREGORIAN, substr($date,5,2), substr($date,0,4));
        $sales["rata"] = round($sales["total"]/$numdays);
        array_push($tabledata, $sales);

        $sales["header"] = "presented";
        for($i = 1; $i < 32; $i++)
        {
            $colname = "d".$i;
            if($tabledata[0][$colname] != 0)
            {
                $temp = ($tabledata[1][$colname] / $tabledata[0][$colname])*100;
                $sales[$colname] = $temp;
            }
            $temp = 0;
        }
        $sales["total"] = ($tabledata[1]["total"]/$tabledata[0]["total"])*100;
        $sales["rata"] = ($tabledata[1]["rata"]/$tabledata[0]["rata"])*100;
        array_push($tabledata, $sales);

        $sales["header"] = "transaksi";
        for($i = 1; $i < 32; $i++)
        {
            $colname = "d".$i;
            foreach($data as $column)
            {
                if(date('d',strtotime($column->date)) == $i)
                {
                    $temp = $temp + 1;
                }
            }
            $sales[$colname] = $temp;
            $temptotal = $temptotal + $temp;
            $temp = 0;
        }
        $sales["total"] = $temptotal;
        $numdays = cal_days_in_month(CAL_GREGORIAN, substr($date,5,2), substr($date,0,4));
        $sales["rata"] = round($sales["total"]/$numdays);
        array_push($tabledata, $sales);
        $therows = "";

        foreach($tabledata as $row => $innerArray)
        {
            $therows .= "<tr>";
            foreach($innerArray as $innerRow => $value)
            {
                if(is_numeric($value))
                {
                    if(is_float($value))
                    {
                        $therows .= "<td style='text-align: right'>".number_format($value,2,".",",")."</td>";
                    }
                    else
                    {
                        $therows .= "<td style='text-align: right'>".number_format($value,0,".",",")."</td>";
                    }
                }
                else
                {
                    $therows .= "<td>".$value."</td>";
                }
            }
            $therows .= "</tr>";
        }

        return $therows;
    }

    public function getSalesMonthDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $date = $request->get('date');

        $data = TDBBranch::ConnectToHeaderWOCashier($branch,$store)
            ->where('trx_headers.trx_date','LIKE',$date)
            ->groupby(\DB::raw('MONTH(trx_headers.trx_date)'))
            ->get(['trx_headers.trx_date as date',\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin'),\DB::raw('count(trx_headers.trx_no) as tcount')]);

        $tabledata = [];
        $sales = [];
        $temp = 0;
        $temptotal = 0;

        $sales["header"] = "TOTAL SALES";
        for($i = 1; $i < 13; $i++)
        {
            $colname = "m".$i;
            foreach($data as $column)
            {
                if(date('m',strtotime($column->date)) == $i)
                {
                    $temp = $temp + $column->sales;
                }
            }
            $sales[$colname] = $temp;
            $temptotal = $temptotal + $temp;
            $temp = 0;
        }
        $sales["total"] = $temptotal;
        $temptotal = 0;
        $sales["rata"] = round($sales["total"]/12);
        array_push($tabledata, $sales);

        $sales["header"] = "TOTAL MARGIN";
        for($i = 1; $i < 13; $i++)
        {
            $colname = "m".$i;
            foreach($data as $column)
            {
                if(date('m',strtotime($column->date)) == $i)
                {
                    $temp = $temp + $column->margin;
                }
            }
            $sales[$colname] = $temp;
            $temptotal = $temptotal + $temp;
            $temp = 0;
        }
        $sales["total"] = $temptotal;
        $temptotal = 0;
        $sales["rata"] = round($sales["total"]/12);
        array_push($tabledata, $sales);

        $sales["header"] = "% MARGIN";
        for($i = 1; $i < 13; $i++)
        {
            $colname = "m".$i;
            if($tabledata[0][$colname] != 0)
            {
                $temp = ($tabledata[1][$colname] / $tabledata[0][$colname])*100;
                $sales[$colname] = $temp;
            }
            $temp = 0;
        }
        $sales["total"] = ($tabledata[1]["total"]/$tabledata[0]["total"])*100;
        $sales["rata"] = ($tabledata[1]["rata"]/$tabledata[0]["rata"])*100;
        array_push($tabledata, $sales);

        $sales["header"] = "JUMLAH TRANSAKSI";
        for($i = 1; $i < 13; $i++)
        {
            $colname = "m".$i;
            foreach($data as $column)
            {
                if(date('m',strtotime($column->date)) == $i)
                {
                    $temp = $temp + $column->tcount;
                }
            }
            $sales[$colname] = $temp;
            $temptotal = $temptotal + $temp;
            $temp = 0;
        }
        $sales["total"] = $temptotal;
        $sales["rata"] = round($sales["total"]/12);
        array_push($tabledata, $sales);
        $therows = "";

        foreach($tabledata as $row => $innerArray)
        {
            $therows .= "<tr>";
            foreach($innerArray as $innerRow => $value)
            {
                if(is_numeric($value))
                {
                    if(is_float($value))
                    {
                        $therows .= "<td style='text-align: right'>".number_format($value,2,".",",")."</td>";
                    }
                    else
                    {
                        $therows .= "<td style='text-align: right'>".number_format($value,0,".",",")."</td>";
                    }
                }
                else
                {
                    $therows .= "<td>".$value."</td>";
                }
            }
            $therows .= "</tr>";
        }

        return $therows;
    }

    public function getSalesProductDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $date = $request->get('date');

        if($date === "daterange")
        {

            $later = new DateTime($request->get('enddate'));
            $numdays = ($later->diff(new DateTime($request->get('startdate')))->format("%a"))+1;

            $startdate = $request->get('startdate')." 00:00:00";
            $enddate = $request->get('enddate')." 23:59:59";

            $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                ->groupBy('user_products.plu')
                ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);
        }
        else
        {
            if($date === "%")
            {
                $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                    ->where('trx_headers.trx_date','LIKE',$date)
                    ->groupBy('user_products.plu')
                    ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('min(trx_headers.trx_date) as mindate'),\DB::raw('max(trx_headers.trx_date) as maxdate'),\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);

                $later = new DateTime(substr($data[0]->maxdate,0,10));
                $numdays = ($later->diff(new DateTime(substr($data[0]->mindate,0,10)))->format("%a"))+1;
            }
            else
            {
                if(substr($date,5,1) === "%")
                {
                    $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                        ->where('trx_headers.trx_date','LIKE',$date)
                        ->groupBy('user_products.plu')
                        ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('min(trx_headers.trx_date) as mindate'),\DB::raw('max(trx_headers.trx_date) as maxdate'),\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);

                    $later = new DateTime(substr($data[0]->maxdate,0,10));
                    $numdays = ($later->diff(new DateTime(substr($data[0]->mindate,0,10)))->format("%a"))+1;
                }
                else
                {
                    $numdays = cal_days_in_month(CAL_GREGORIAN, substr($date,5,2), substr($date,0,4));

                    $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                        ->where('trx_headers.trx_date','LIKE',$date)
                        ->groupBy('user_products.plu')
                        ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);
                }
            }
        }
        $div=[];
        $dep=[];
        $kat=[];
        foreach($data as $row => $item)
        {
            if($item->category != null)
            {
                $temp = Margin::join('divisi','master_margin.div','=','divisi.DIV_KODEDIVISI')
                    ->join('department', function ($join) {
                        $join->on('master_margin.dep','=','department.DEP_KODEDEPARTEMENT');
                        $join->on('divisi.DIV_KODEDIVISI','=','department.DEP_KODEDIVISI');
                    })
                    ->join('category', function ($join) {
                        $join->on('master_margin.kat','=','category.KAT_KODEKATEGORI');
                        $join->on('department.DEP_KODEDEPARTEMENT','=','category.KAT_KODEDEPARTEMENT');
                    })
                    ->where('master_margin.id','=',$item->category)
                    ->get(['divisi.DIV_KODEDIVISI as div','department.DEP_KODEDEPARTEMENT as dep','category.KAT_KODEKATEGORI as kat']);
                $div[] = $temp[0]->div;
                $dep[] = $temp[0]->dep;
                $kat[] = $temp[0]->kat;
            }
            else
            {
                $div[] = '-';
                $dep[] = '-';
                $kat[] = '-';
            }
        }
        $therows = "";
        $i=0;
        foreach($data as $row => $value)
        {
            $therows .= "<tr>";
            $therows .= "<td>".$div[$i]."</td>";
            $therows .= "<td>".$dep[$i]."</td>";
            $therows .= "<td>".$kat[$i]."</td>";
            $therows .= "<td>".$value->plu."</td>";
            $therows .= "<td>".$value->description."</td>";
            $therows .= "<td style='text-align: right'>".number_format($value->qty,0,",",",")."</td>";
            $therows .= "<td style='text-align: right'>".number_format($value->subtotal,0,",",",")."</td>";
            $therows .= "<td style='text-align: right'>".number_format($value->margin,0,",",",")."</td>";
            $therows .= "<td style='text-align: right'>".number_format(($value->margin/$value->subtotal)*100,2,",",",")."</td>";
            if($value->qty != null)
            {
                $therows .= "<td>".$value->qty/$numdays."</td>";
            }
            else
            {
                $therows .= "<td>0</td>";
            }
            $therows .= "<td>".$value->invcount."</td>";
            $therows .= "</tr>";
            $i++;
        }

        return $therows;
    }

    public function getSalesBranchDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $date = $request->get('date');

        $data = TDBBranch::ConnectToHeaderWOCashier($branch,$store)
            ->where('trx_headers.trx_date','LIKE',$date)
            ->groupBy('users.store_name',\DB::raw('MONTH(trx_headers.trx_date)'))
            ->get(['users.store_name','trx_headers.trx_date',\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin')]);

        $stores = $data->unique('store_name');

        $therows = "";
        $ts = 0;
        $tm = 0;
        foreach($stores as $row)
        {
            $values = [];
            $therows .= "<tr>";
            $therows .= "<td>".$row->store_name."</td>";
            foreach($data as $cell)
            {
                if($row->store_name == $cell->store_name)
                {
                    $month = (int)substr($cell->trx_date,5,2);
                    $values[$month-1] = array($cell->sales,$cell->margin);
                }
            }
            for($i = 0;$i<12;$i++)
            {
                if($values[$i] != null)
                {
                    $therows .= "<td>".number_format($values[$i][0],2,",",",")."</td>";
                    $therows .= "<td>".number_format($values[$i][1],2,",",",")."</td>";
                    $ts = $ts + $values[$i][0];
                    $tm = $tm + $values[$i][1];
                }
                else
                {
                    $therows .= "<td>0</td>";
                    $therows .= "<td>0</td>";
                }
            }
            $therows .= "<td>".number_format($ts,2,",",",")."</td>";
            $therows .= "<td>".number_format($tm,2,",",",")."</td>";
            $therows .= "</tr>";
            $ts = 0;
            $tm = 0;
            $values = null;
        }

        return $therows;
    }

    public function getSalesRecapDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $date = $request->get('date');

        if($date === "daterange")
        {
            $startdate = $request->get('startdate')." 00:00:00";
            $enddate = $request->get('enddate')." 23:59:59";

            $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                ->groupBy('users.id','products.id')
                ->get(['users.store_name as storename','products.id as pid',\DB::raw('sum(trx_details.sub_total) as sales'),\DB::raw('sum(trx_details.margin) as margin')]);

            $days = TDBUserProduct::ConnectForFreeplu($store,$branch)
                ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                ->groupBy('users.id')
                ->get(['users.store_name as storename',\DB::raw('count(distinct day(trx_headers.trx_date)) as days')]);
        }
        else
        {
            $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->groupBy('users.id','products.id')
                ->get(['users.store_name as storename','products.id as pid',\DB::raw('sum(trx_details.sub_total) as sales'),\DB::raw('sum(trx_details.margin) as margin')]);

            $days = TDBUserProduct::ConnectForFreeplu($store,$branch)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->groupBy('users.id')
                ->get(['users.store_name as storename',\DB::raw('count(distinct day(trx_headers.trx_date)) as days')]);
        }

        $stores = $data->unique('storename');

        $therows = "";
        $ts = 0;
        foreach($stores as $row)
        {
            $values = [];
            $therows .= "<tr>";
            $therows .= "<td>".$row->storename."</td>";
            foreach($days as $cell)
            {
                if($row->storename === $cell->storename)
                {
                    $therows .= "<td>".$cell->days."</td>";
                }
            }

            foreach($data as $cell)
            {
                if($row->storename === $cell->storename)
                {
                    if($cell->pid != null)
                    {
                        $values[0][0] = $values[0][0] + $cell->sales;
                        $values[1][0] = $values[1][0] + $cell->margin;
                    }
                    else
                    {
                        $values[0][1] = $values[0][1] + $cell->sales;
                        $values[1][1] = $values[1][1] + $cell->margin;
                    }
                }
            }
            for($i = 0;$i<2;$i++)
            {
                for($j = 0; $j<2;$j++)
                {
                    if($values[$i][$j] != null)
                    {
                        $therows .= "<td>".number_format($values[$i][$j],0,",",",")."</td>";
                        $ts = $ts + $values[$i][$j];
                    }
                    else
                    {
                        $therows .= "<td>0</td>";
                    }
                }
                $therows .= "<td>".number_format($ts,0,",",",")."</td>";
                $ts = 0;
            }
            $therows .= "<td>".number_format((($values[1][0]/$values[0][0])*100),2,",",",")."</td>";
            $therows .= "<td>".number_format((($values[1][0]/$values[0][0])*100),2,",",",")."</td>";
            $therows .= "<td>".number_format(((($values[1][0]+$values[1][1])/($values[0][0]+$values[0][1]))*100),2,",",",")."</td>";
            $therows .= "</tr>";
            $values = null;
        }
        return $therows;
    }

    public function getSalesBranchDateDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $date = $request->get('date');

        if($date === "daterange")
        {
            $startdate = $request->get('startdate')." 00:00:00";
            $enddate = $request->get('enddate')." 23:59:59";

            $data = TDBBranch::ConnectToHeaderWithTmiType($branch,$store)
                ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                ->groupBy(\DB::raw('date(trx_headers.trx_date)'),'users.id')
                ->get(['branches.name as branch','users.store_name as store','tmi_types.description as type',
                    \DB::raw("(select date(min(trx_headers.trx_date)) from branches join users on users.branch_id = branches.id join trx_headers on trx_headers.user_id = users.id where users.store_name = store) as opendate"),
                    \DB::raw('date(trx_headers.trx_date) as tdate'),\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin'),\DB::raw('count(distinct trx_headers.id) as invcount')]);
        }
        else
        {
            $data = TDBBranch::ConnectToHeaderWithTmiType($branch,$store)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->groupBy(\DB::raw('date(trx_headers.trx_date)'),'users.id')
                ->get(['branches.name as branch','users.store_name as store','tmi_types.description as type',
                    \DB::raw("(select date(min(trx_headers.trx_date)) from branches join users on users.branch_id = branches.id join trx_headers on trx_headers.user_id = users.id where users.store_name = store) as opendate"),
                    \DB::raw('date(trx_headers.trx_date) as tdate'),\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin'),\DB::raw('count(distinct trx_headers.id) as invcount')]);
        }

        $therows = "";
        foreach($data as $row => $value)
        {
            $therows .= "<tr>";
            $therows .= "<td>".$value->branch."</td>";
            $therows .= "<td>".$value->store."</td>";
            $therows .= "<td>".$value->type."</td>";
            $therows .= "<td>".$value->opendate."</td>";
            $therows .= "<td>".$value->tdate."</td>";
            $therows .= "<td>".number_format($value->sales,0,",",",")."</td>";
            $therows .= "<td>".$value->invcount."</td>";
            $therows .= "<td style='text-align: right'>".number_format($value->sales/$value->invcount,0,",",",")."</td>";
            $therows .= "<td style='text-align: right'>".number_format(($value->margin/$value->sales)*100,2,",",",")."</td>";
            $therows .= "</tr>";
        }
        return $therows;
    }


    public function getParetoDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $type = $request->get('type');
        $date = $request->get('date');

        if($type === "cnt")
        {
            $arr = ['branches.name as branch','users.store_name as store','user_products.description as product',\DB::raw('count(distinct(trx_details.trx_header_id)) as total')];
        }
        else if($type === "qty")
        {
            $arr = ['branches.name as branch','users.store_name as store','user_products.description as product',\DB::raw('sum(trx_details.qty) as total')];
        }
        else if($type === "prc")
        {
            $arr = ['branches.name as branch','users.store_name as store','user_products.description as product',\DB::raw('sum(trx_details.price*trx_details.qty) as total')];
        }

        if($date === "daterange")
        {
            $startdate = $request->get('startdate')." 00:00:00";
            $enddate = $request->get('enddate')." 23:59:59";

            $data = TDBBranch::ConnectToDetail($branch,$store)
                ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                ->groupBy('user_products.id')
                ->get($arr);
        }
        else
        {
            $data = TDBBranch::ConnectToDetail($branch,$store)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->groupBy('user_products.id')
                ->get($arr);
        }

        return \Datatables::of($data)->make(true);
    }

    public function getPbDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $date = $request->get('date');

        if($date === "daterange")
        {
            $startdate = $request->get('startdate');
            $enddate = $request->get('enddate');

            $data = TDBBranch::ConnectToPb($branch,$store)
                ->whereBetween('pb_headers.po_date',[$startdate,$enddate])
                ->get(['branches.name as branch','users.store_name as store','pb_headers.po_number as po_no','pb_headers.po_date as po_date','pb_headers.name as name','pb_headers.email as email','pb_headers.phone_number as phone','pb_headers.address as address','pb_headers.qty_order as qtyo','pb_headers.qty_fulfilled as qtyf','pb_headers.price_order as priceo','pb_headers.price_fulfilled as pricef','pb_headers.flag_free_delivery as isfree','pb_statuses.title as status','pb_headers.flag_sent as issent']);
        }
        else
        {
            $data = TDBBranch::ConnectToPb($branch,$store)
                ->where('pb_headers.po_date','LIKE',$date)
                ->get(['branches.name as branch','users.store_name as store','pb_headers.po_number as po_no','pb_headers.po_date as po_date','pb_headers.name as name','pb_headers.email as email','pb_headers.phone_number as phone','pb_headers.address as address','pb_headers.qty_order as qtyo','pb_headers.qty_fulfilled as qtyf','pb_headers.price_order as priceo','pb_headers.price_fulfilled as pricef','pb_headers.flag_free_delivery as isfree','pb_statuses.title as status','pb_headers.flag_sent as issent']);
        }

        return \Datatables::of($data)->make(true);
    }

    public function getPromoDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');
        $date = $request->get('date');

        if($date === "daterange")
        {
            $startdate = $request->get('startdate');
            $enddate = $request->get('enddate');

            $data = TDBBranch::ConnectToPromotion($branch,$store)
                ->where('promotions.start_date','>=',$startdate)
                ->where('promotions.end_date','<=',$enddate)
                ->get(['branches.name as branch','users.store_name as store','user_products.description as name','promotions.mechanism as promo','promotions.discount as disc','promotions.min_purchase as min','promotions.start_date as startdate','promotions.end_date as enddate']);
        }
        else
        {
            $data = TDBBranch::ConnectToPromotion($branch,$store)
                ->get(['branches.name as branch','users.store_name as store','user_products.description as name','promotions.mechanism as promo','promotions.discount as disc','promotions.min_purchase as min','promotions.start_date as startdate','promotions.end_date as enddate']);
        }

        return \Datatables::of($data)->make(true);
    }

    public function getArchiveDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');

        $data = TDBBranch::ConnectToUserProducts($branch,$store)
            ->whereNotNull('user_products.deleted_at')
            ->get(['branches.name as branch','users.store_name as store','user_products.plu as plu','user_products.description as desc','user_products.unit as unit','user_products.fraction as frac','user_products.deleted_at as date']);

        return \Datatables::of($data)->make(true);
    }

    public function getStockDatatable(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');

        $data = TDBBranch::ConnectUserProductToDiv($branch,$store)
            ->whereNull('user_products.deleted_at')
            ->get(['categories.category as category','user_products.plu as plu','user_products.id as pid','user_products.description as desc',
                \DB::raw('(select sum(trx_details.qty)/30
                    from users
                    join user_products on users.id = user_products.user_id
                    join trx_details on user_products.id = trx_details.user_product_id
                    join trx_headers on trx_headers.id = trx_details.trx_header_id
                    where users.id = '.$store.' and user_products.id = pid and date(trx_headers.trx_date) between (curdate() - interval 30 day) and curdate()) as avgsales'),
                'user_products.stock as stock','user_products.min_stock as minq','user_products.max_stock as maxq','user_products.deleted_at as date']);

        $div=[];
        $dep=[];
        $kat=[];
        foreach($data as $row => $item)
        {
            $temp = Margin::join('divisi','master_margin.div','=','divisi.DIV_KODEDIVISI')
                ->join('department', function ($join) {
                    $join->on('master_margin.dep','=','department.DEP_KODEDEPARTEMENT');
                    $join->on('divisi.DIV_KODEDIVISI','=','department.DEP_KODEDIVISI');
                })
                ->join('category', function ($join) {
                    $join->on('master_margin.kat','=','category.KAT_KODEKATEGORI');
                    $join->on('department.DEP_KODEDEPARTEMENT','=','category.KAT_KODEDEPARTEMENT');
                })
                ->where('master_margin.id','=',$item->category)
                ->get(['divisi.DIV_KODEDIVISI as div','department.DEP_KODEDEPARTEMENT as dep','category.KAT_KODEKATEGORI as kat']);
            $div[] = $temp[0]->div;
            $dep[] = $temp[0]->dep;
            $kat[] = $temp[0]->kat;
        }
        $therows = "";
        $i=0;
        foreach($data as $row => $value)
        {
            $therows .= "<tr>";
            $therows .= "<td>".$div[$i]."</td>";
            $therows .= "<td>".$dep[$i]."</td>";
            $therows .= "<td>".$kat[$i]."</td>";
            $therows .= "<td>".$value->plu."</td>";
            $therows .= "<td>".$value->desc."</td>";
            $therows .= "<td style='text-align: right'>".$value->stock."</td>";
            if($value->avgsales != null)
            {
                $therows .= "<td style='text-align: right'>".$value->avgsales."</td>";
            }
            else
            {
                $therows .= "<td style='text-align: right'>0</td>";
            }
            $therows .= "<td style='text-align: right'>MIN DIS</td>";
            $therows .= "<td style='text-align: right'>".$value->minq."</td>";
            $therows .= "<td style='text-align: right'>".$value->maxq."</td>";
            $therows .= "</tr>";
            $i++;
        }

        return $therows;
    }

    public function exportSales(Request $request)
    {
        $branch = $request->get('efcabang');
        $store = $request->get('eftoko');
        $cashier = $request->get('efkasir');
        $date = $request->get('efhari');
        $type = $request->get('eftipe');
        $months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $numdays = cal_days_in_month(CAL_GREGORIAN, substr($date,5,2), substr($date,0,4));

        if($type === "day")
        {
            $data = TDBBranch::ConnectToHeader($branch,$store,$cashier)
            ->where('trx_headers.trx_date','LIKE',$date)
            ->get(['branches.name as branch','users.store_name as store','trx_headers.trx_no as invoice','trx_headers.trx_date as date','trx_headers.grand_total as total','trx_headers.margin as margin']);

            $branchname = $data[0]->branch;
            $storename = $data[0]->store;
            $tabledata = [];
            $sales = [];
            $temp = 0;
            $temptotal = 0;

            $sales["header"] = "sales";
            for($i = 1; $i < 32; $i++)
            {
                $colname = "d".$i;
                foreach($data as $column)
                {
                    if(date('d',strtotime($column->date)) == $i)
                    {
                        $temp = $temp + $column->total;
                    }
                }
                $sales[$colname] = $temp;
                $temptotal = $temptotal + $temp;
                $temp = 0;
            }
            $sales["total"] = $temptotal;
            $temptotal = 0;
            $sales["rata"] = round($sales["total"]/$numdays);
            array_push($tabledata, $sales);

            $sales["header"] = "margin (rupiah)";
            for($i = 1; $i < 32; $i++)
            {
                $colname = "d".$i;
                foreach($data as $column)
                {
                    if(date('d',strtotime($column->date)) == $i)
                    {
                        $temp = $temp + $column->margin;
                    }
                }
                $sales[$colname] = $temp;
                $temptotal = $temptotal + $temp;
                $temp = 0;
            }
            $sales["total"] = $temptotal;
            $temptotal = 0;
            $sales["rata"] = round($sales["total"]/$numdays);
            array_push($tabledata, $sales);

            $sales["header"] = "presented";
            for($i = 1; $i < 32; $i++)
            {
                $colname = "d".$i;
                if($tabledata[0][$colname] != 0)
                {
                    $temp = round(($tabledata[1][$colname] / $tabledata[0][$colname])*100);
                    $sales[$colname] = $temp;
                }
                $temp = 0;
            }
            $sales["total"] = ($tabledata[1]["total"]/$tabledata[0]["total"])*100;
            $sales["rata"] = ($tabledata[1]["rata"]/$tabledata[0]["rata"])*100;
            array_push($tabledata, $sales);

            $sales["header"] = "transaksi";
            for($i = 1; $i < 32; $i++)
            {
                $colname = "d".$i;
                foreach($data as $column)
                {
                    if(date('d',strtotime($column->date)) == $i)
                    {
                        $temp = $temp + 1;
                    }
                }
                $sales[$colname] = $temp;
                $temptotal = $temptotal + $temp;
                $temp = 0;
            }
            $sales["total"] = $temptotal;
            $sales["rata"] = round($sales["total"]/$numdays);
            array_push($tabledata, $sales);

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'LAPORAN SALES PERDAY');
            $sheet->setCellValue('A3', 'Cabang :');
            $sheet->setCellValue('B3', $branchname);
            $sheet->setCellValue('A4', 'TMI :');
            if($store === "%")
            {
                $sheet->setCellValue('B4', 'SEMUA TOKO');
            }
            else
            {
                $sheet->setCellValue('B4', $storename);
            }
            $monthnow = (int)substr($date,5,2);
            $sheet->setCellValue('A5', 'PERIODE :');
            $sheet->setCellValue('B5', $months[$monthnow-1].' '.substr($date,0,4));
            $sheet->setCellValue('A7', 'DESKRIPSI');
            $sheet->setCellValue('B7', $months[$monthnow-1].' '.substr($date,0,4));
            $sheet->setCellValue('AG7', 'TOTAL');
            $sheet->setCellValue('AH7', 'RATA-RATA');
            $sheet->mergeCells("A7:A8");
            $sheet->mergeCells("AG7:AG8");
            $sheet->mergeCells("AH7:AH8");
            $sheet->mergeCells("B7:AF7");

            $sheet->fromArray($tabledata,NULL,'A9');

            $i = 1;
            $lastColumn = 'AF';
            $lastColumn++;
            for ($currentColumn = 'B'; $currentColumn != $lastColumn; $currentColumn++) {
                $sheet->setCellValue($currentColumn.'8', $i);
                $sheet->getStyle($currentColumn.'8')->getAlignment()->setHorizontal('center');
                $i++;
            }

            for ($currentColumn = 'B'; $currentColumn != $lastColumn; $currentColumn++) {
                $sheet->setCellValue($currentColumn.'11', '=('.$currentColumn.'10/'.$currentColumn.'9)*100');
            }

            $sheet->setCellValue('AG11', '=SUM(B11:AF11)');
            $sheet->setCellValue('AH11', '=AVERAGE(B11:AF11)');

            $sheet->getStyle('B7')->getAlignment()->setHorizontal('center');

            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('AG')->setAutoSize(true);
            $sheet->getColumnDimension('AH')->setAutoSize(true);
            $sheet->getDefaultRowDimension()->setRowHeight(-1);

            $sheet->setTitle("sales per day");

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="salesperday.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');

        }
        else if($type === "month")
        {
            $branch = $request->get('efcabang');
            $store = $request->get('eftoko');
            $date = $request->get('efhari');

            $data = TDBBranch::ConnectToHeaderWOCashier($branch,$store)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->groupby(\DB::raw('MONTH(trx_headers.trx_date)'))
                ->get(['branches.name as branch','users.store_name as store','trx_headers.trx_date as date',\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin'),\DB::raw('count(trx_headers.trx_no) as tcount')]);

            $branchname = $data[0]->branch;
            $storename = $data[0]->store;
            $tabledata = [];
            $sales = [];
            $temp = "0";
            $temptotal = "0";

            $sales["header"] = "TOTAL SALES";
            for($i = 1; $i < 13; $i++)
            {
                $colname = "m".$i;
                foreach($data as $column)
                {
                    if(date('m',strtotime($column->date)) == $i)
                    {
                        $temp = $temp + $column->sales;
                    }
                }
                $sales[$colname] = $temp;
                $temptotal = $temptotal + $temp;
                $temp = 0;
            }
            $sales["total"] = $temptotal;
            $temptotal = 0;
            $sales["rata"] = round($sales["total"]/12);
            array_push($tabledata, $sales);

            $sales["header"] = "TOTAL MARGIN";
            for($i = 1; $i < 13; $i++)
            {
                $colname = "m".$i;
                foreach($data as $column)
                {
                    if(date('m',strtotime($column->date)) == $i)
                    {
                        $temp = $temp + $column->margin;
                    }
                }
                $sales[$colname] = $temp;
                $temptotal = $temptotal + $temp;
                $temp = 0;
            }
            $sales["total"] = $temptotal;
            $temptotal = 0;
            $sales["rata"] = round($sales["total"]/12);
            array_push($tabledata, $sales);

            $sales["header"] = "% MARGIN";
            for($i = 1; $i < 13; $i++)
            {
                $colname = "m".$i;
                if($tabledata[0][$colname] != 0)
                {
                    $temp = ($tabledata[1][$colname] / $tabledata[0][$colname])*100;
                    $sales[$colname] = $temp;
                }
                $temp = 0;
            }
            $sales["total"] = ($tabledata[1]["total"]/$tabledata[0]["total"])*100;
            $sales["rata"] = ($tabledata[1]["rata"]/$tabledata[0]["rata"])*100;
            array_push($tabledata, $sales);

            $sales["header"] = "JUMLAH TRANSAKSI";
            for($i = 1; $i < 13; $i++)
            {
                $colname = "m".$i;
                foreach($data as $column)
                {
                    if(date('m',strtotime($column->date)) == $i)
                    {
                        $temp = $temp + $column->tcount;
                    }
                }
                $sales[$colname] = $temp;
                $temptotal = $temptotal + $temp;
                $temp = 0;
            }
            $sales["total"] = $temptotal;
            $sales["rata"] = round($sales["total"]/12);
            array_push($tabledata, $sales);

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'REKAP SALES DETAIL');
            $sheet->setCellValue('A3', 'Cabang :');
            $sheet->setCellValue('B3', $branchname);
            $sheet->setCellValue('A4', 'TMI :');
            if($store === "%")
            {
                $sheet->setCellValue('B4', 'SEMUA TOKO');
            }
            else
            {
                $sheet->setCellValue('B4', $storename);
            }
            $sheet->setCellValue('A5', 'PERIODE :');
            $sheet->setCellValue('B5', 'TAHUN '.substr($date,0,4));

            $sheet->setCellValue('A7', 'TANGGAL');
            $sheet->setCellValue('B7', 'JANUARI');
            $sheet->setCellValue('C7', 'FEBRUARI');
            $sheet->setCellValue('D7', 'MARET');
            $sheet->setCellValue('E7', 'APRIL');
            $sheet->setCellValue('F7', 'MEI');
            $sheet->setCellValue('G7', 'JUNI');
            $sheet->setCellValue('H7', 'JULI');
            $sheet->setCellValue('I7', 'AGUSTUS');
            $sheet->setCellValue('J7', 'SEPTEMBER');
            $sheet->setCellValue('K7', 'OKTOBER');
            $sheet->setCellValue('L7', 'NOVEMBER');
            $sheet->setCellValue('M7', 'DESEMBER');
            $sheet->setCellValue('N7', 'TOTAL');
            $sheet->setCellValue('O7', 'RATA-RATA');

            $sheet->fromArray($tabledata,NULL,'A8');

            $lastColumn = 'P';
            for ($currentColumn = 'A'; $currentColumn != $lastColumn; $currentColumn++) {
                $sheet->getColumnDimension($currentColumn)->setAutoSize(true);
            }
            $sheet->setCellValue('N10', '=SUM(B10:M10)');
            $sheet->setCellValue('O10', '=AVERAGE(B10:M10)');

            $sheet->getStyle('B7')->getAlignment()->setHorizontal('center');


            $sheet->getDefaultRowDimension()->setRowHeight(-1);

            $sheet->setTitle("sales per month");

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="salespermonth.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');

        }
        else if($type === "product")
        {
            $branch = $request->get('efcabang');
            $store = $request->get('eftoko');
            $date = $request->get('efhari');

            if($date === "daterange")
            {

                $later = new DateTime($request->get('enddate'));
                $numdays = ($later->diff(new DateTime($request->get('startdate')))->format("%a"))+1;

                $startdate = $request->get('startdate')." 00:00:00";
                $enddate = $request->get('enddate')." 23:59:59";

                $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                    ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                    ->groupBy('user_products.plu')
                    ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);
            }
            else
            {
                if($date === "%")
                {
                    $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                        ->where('trx_headers.trx_date','LIKE',$date)
                        ->groupBy('user_products.plu')
                        ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('min(trx_headers.trx_date) as mindate'),\DB::raw('max(trx_headers.trx_date) as maxdate'),\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);

                    $later = new DateTime(substr($data[0]->maxdate,0,10));
                    $numdays = ($later->diff(new DateTime(substr($data[0]->mindate,0,10)))->format("%a"))+1;
                }
                else
                {
                    if(substr($date,5,1) === "%")
                    {
                        $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                            ->where('trx_headers.trx_date','LIKE',$date)
                            ->groupBy('user_products.plu')
                            ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('min(trx_headers.trx_date) as mindate'),\DB::raw('max(trx_headers.trx_date) as maxdate'),\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);

                        $later = new DateTime(substr($data[0]->maxdate,0,10));
                        $numdays = ($later->diff(new DateTime(substr($data[0]->mindate,0,10)))->format("%a"))+1;
                    }
                    else
                    {
                        $numdays = cal_days_in_month(CAL_GREGORIAN, substr($date,5,2), substr($date,0,4));

                        $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                            ->where('trx_headers.trx_date','LIKE',$date)
                            ->groupBy('user_products.plu')
                            ->get(['user_products.id as pid','user_products.plu as plu','categories.category as category','user_products.description as description',\DB::raw('sum(trx_details.qty) as qty'),\DB::raw('sum(trx_details.sub_total) as subtotal'),\DB::raw('sum(trx_details.margin) as margin'),\DB::raw('count(distinct trx_details.trx_header_id) as invcount')]);
                    }
                }
            }
            $div=[];
            $dep=[];
            $kat=[];
            foreach($data as $row => $item)
            {
                if($item->category != null)
                {
                    $temp = Margin::join('divisi','master_margin.div','=','divisi.DIV_KODEDIVISI')
                        ->join('department', function ($join) {
                            $join->on('master_margin.dep','=','department.DEP_KODEDEPARTEMENT');
                            $join->on('divisi.DIV_KODEDIVISI','=','department.DEP_KODEDIVISI');
                        })
                        ->join('category', function ($join) {
                            $join->on('master_margin.kat','=','category.KAT_KODEKATEGORI');
                            $join->on('department.DEP_KODEDEPARTEMENT','=','category.KAT_KODEDEPARTEMENT');
                        })
                        ->where('master_margin.id','=',$item->category)
                        ->get(['divisi.DIV_KODEDIVISI as div','department.DEP_KODEDEPARTEMENT as dep','category.KAT_KODEKATEGORI as kat']);
                    $div[] = $temp[0]->div;
                    $dep[] = $temp[0]->dep;
                    $kat[] = $temp[0]->kat;
                }
                else
                {
                    $div[] = '-';
                    $dep[] = '-';
                    $kat[] = '-';
                }
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'LAPORAN SALES PER PRODUK');
            $sheet->setCellValue('A2', 'CABANG');
            $sheet->setCellValue('A3', 'TOKO');
            $sheet->setCellValue('A4', 'PERIODE');

            $branchname = TDBBranch::where('id','=',$branch)->get(['name']);
            $sheet->setCellValue('B2', $branchname[0]->name);

            if($store === '%')
            {
                $sheet->setCellValue('B3', 'SEMUA TOKO');
            }
            else
            {
                $storename = TDBUser::where('id','=',$store)->get(['store_name']);
                $sheet->setCellValue('B3', $storename[0]->store_name);
            }

            if($date === 'daterange')
            {
                $sheet->setCellValue('B4', $request->get('startdate').' s/d '.$request->get('enddate'));
            }
            else if($date === '%')
            {
                $sheet->setCellValue('B4', 'ALL TIME');
            }
            else
            {
                $sheet->setCellValue('B4', substr($date,0,4).' '.$months[(int)substr($date,5,2)]);
            }

            $sheet->setCellValue('A6', 'DIV');
            $sheet->setCellValue('B6', 'DEP');
            $sheet->setCellValue('C6', 'KAT');
            $sheet->setCellValue('D6', 'PLU');
            $sheet->setCellValue('E6', 'DESKRIPSI');
            $sheet->setCellValue('F6', 'SALES QTY');
            $sheet->setCellValue('G6', 'SALES RPH');
            $sheet->setCellValue('H6', 'MGN RPH');
            $sheet->setCellValue('I6', 'MGN %');
            $sheet->setCellValue('J6', 'SPD');
            $sheet->setCellValue('K6', 'JML TRANSAKSI');
            $sheet->getStyle('A6:K6')->getAlignment()->setHorizontal('center');

            $no = 7;
            foreach($data as $row)
            {
                $sheet->setCellValue('A'.$no, $div[$no-7]);
                $sheet->setCellValue('B'.$no, $dep[$no-7]);
                $sheet->getStyle('B'.$no)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                $sheet->setCellValue('C'.$no, $kat[$no-7]);
                $sheet->getStyle('C'.$no)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                $sheet->setCellValue('D'.$no, $row->plu);
                $sheet->getStyle('D'.$no)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                $sheet->setCellValue('E'.$no, $row->description);
                $sheet->setCellValue('F'.$no, $row->qty);
                $sheet->setCellValue('G'.$no, $row->subtotal);
                $sheet->setCellValue('H'.$no, $row->margin);
                $sheet->setCellValue('I'.$no, '=(H'.$no.'/G'.$no.')*100');
                if($row->avgsales != null)
                {
                    $sheet->setCellValue('J'.$no, $row->avgsales);
                }
                else
                {
                    $sheet->setCellValue('J'.$no, '0');
                }
                $sheet->setCellValue('K'.$no, $row->invcount);
                $no++;
            }

            $lastColumn='L';
            for ($currentColumn = 'A'; $currentColumn != $lastColumn; $currentColumn++)
            {
                $sheet->getColumnDimension($currentColumn)->setAutoSize(true);
            }
            $sheet->getDefaultRowDimension()->setRowHeight(-1);

            $sheet->setTitle("sales per item");

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="salesperitem.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
        }
        else if($type === "branch")
        {
            $branch = $request->get('efcabang');
            $store = $request->get('eftoko');
            $date = $request->get('efhari');

            $data = TDBBranch::ConnectToHeaderWOCashier($branch,$store)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->groupBy('users.store_name',\DB::raw('MONTH(trx_headers.trx_date)'))
                ->get(['users.store_name','trx_headers.trx_date','trx_headers.grand_total as sales','trx_headers.margin as margin']);

            $stores = $data->unique('store_name');

            $tabledata = [];
            $ts = 0;
            $tm = 0;
            foreach($stores as $row)
            {
                $values = [];
                $storerow = [];
                $storerow[] = $row->store_name;
                foreach($data as $cell)
                {
                    if($row->store_name === $cell->store_name)
                    {
                        $month = (int)substr($cell->trx_date,5,2);
                        $values[$month-1] = array($cell->sales,$cell->margin);
                    }
                }
                for($i = 0;$i<12;$i++)
                {
                    if($values[$i] != null)
                    {
                        $storerow[] = $values[$i][0];
                        $storerow[] = $values[$i][1];
                        $ts = $ts + $values[$i][0];
                        $tm = $tm + $values[$i][1];
                    }
                    else
                    {
                        $storerow[] = "0";
                        $storerow[] = "0";
                    }
                }
                $storerow[] = $ts;
                $storerow[] = $tm;
                array_push($tabledata, $storerow);
                $ts = 0;
                $tm = 0;
                $values = null;
                $storerow = null;
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'LAPORAN SALES PER CABANG');
            $sheet->setCellValue('A3', 'CABANG');
            $sheet->setCellValue('A4', 'PERIODE');

            if($branch === '%')
            {
                $sheet->setCellValue('B3', 'SEMUA CABANG');
            }
            else
            {
                $branchname = TDBBranch::where('id','=',$branch)->get(['name']);
                $sheet->setCellValue('B3', $branchname[0]->name);
            }

            $sheet->setCellValue('B4', 'TAHUN '.substr($date,0,4));

            $sheet->setCellValue('A6', 'TMI');
            $sheet->setCellValue('B6', 'Januari');
            $sheet->setCellValue('D6', 'Februari');
            $sheet->setCellValue('F6', 'Maret');
            $sheet->setCellValue('H6', 'April');
            $sheet->setCellValue('J6', 'Mei');
            $sheet->setCellValue('L6', 'Juni');
            $sheet->setCellValue('N6', 'Juli');
            $sheet->setCellValue('P6', 'Agustus');
            $sheet->setCellValue('R6', 'September');
            $sheet->setCellValue('T6', 'Oktober');
            $sheet->setCellValue('V6', 'November');
            $sheet->setCellValue('X6', 'Desember');
            $sheet->setCellValue('Z6', 'Total');
            $lastColumn = 'AB';
            $helper = 2;
            for ($currentColumn = 'B'; $currentColumn != $lastColumn; $currentColumn++)
            {
                if($helper % 2 == 0)
                {
                    $sheet->mergeCells($currentColumn.'6:'.($this->incrementLetter($currentColumn, 1)).'6');
                    $sheet->setCellValue($currentColumn.'7', 'sales');
                    $sheet->setCellValue(($this->incrementLetter($currentColumn, 1)).'7', 'margin');
                    $sheet->getStyle($currentColumn.'7')->getAlignment()->setHorizontal('center');
                    $sheet->getStyle(($this->incrementLetter($currentColumn, 1)).'7')->getAlignment()->setHorizontal('center');
                }
                $helper++;
            }
            $sheet->getStyle('A6:AA7')->getAlignment()->setHorizontal('center');

            $sheet->fromArray($tabledata,NULL,'A8');

            $lastColumn='AB';
            for ($currentColumn = 'A'; $currentColumn != $lastColumn; $currentColumn++)
            {
                $sheet->getColumnDimension($currentColumn)->setAutoSize(true);
            }
            $sheet->getDefaultRowDimension()->setRowHeight(-1);

            $sheet->setTitle("sales per branch");

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="salesperbranch.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
        }
        else if($type === "recap")
        {
            $store = $request->get('eftoko');
            $date = $request->get('efhari');

            if($date === "daterange")
            {
                $startdate = $request->get('startdate')." 00:00:00";
                $enddate = $request->get('enddate')." 23:59:59";

                $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                    ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                    ->groupBy('users.id','products.id')
                    ->get(['users.store_name as storename','products.id as pid',\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin')]);

                $days = TDBUserProduct::ConnectForFreeplu($store,$branch)
                    ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                    ->groupBy('users.id')
                    ->get(['users.store_name as storename',\DB::raw('count(distinct day(trx_headers.trx_date)) as days')]);
            }
            else
            {
                $data = TDBUserProduct::ConnectForFreeplu($store,$branch)
                    ->where('trx_headers.trx_date','LIKE',$date)
                    ->groupBy('users.id','products.id')
                    ->get(['users.store_name as storename','products.id as pid',\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin')]);

                $days = TDBUserProduct::ConnectForFreeplu($store,$branch)
                    ->where('trx_headers.trx_date','LIKE',$date)
                    ->groupBy('users.id')
                    ->get(['users.store_name as storename',\DB::raw('count(distinct day(trx_headers.trx_date)) as days')]);
            }

            $stores = $data->unique('storename');
            $ts = 0;

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'REKAP SALES DETAIL');
            $sheet->setCellValue('A3', 'CABANG');
            $sheet->setCellValue('A4', 'PERIODE');

            if($branch === '%')
            {
                $sheet->setCellValue('B3', 'SEMUA CABANG');
            }
            else
            {
                $branchname = TDBBranch::where('id','=',$branch)->get(['name']);
                $sheet->setCellValue('B3', $branchname[0]->name);
            }

            $sheet->setCellValue('B4', 'TAHUN '.substr($date,0,4));

            $sheet->setCellValue('A6', 'NAMA TOKO');
            $sheet->mergeCells("A6:A7");
            $sheet->setCellValue('B6', 'HARI BUKA');
            $sheet->mergeCells("B6:B7");
            $sheet->setCellValue('C6', 'SALES');
            $sheet->mergeCells("C6:E6");
            $sheet->setCellValue('F6', 'MARGIN RUPIAH');
            $sheet->mergeCells("F6:H6");
            $sheet->setCellValue('I6', 'MARGIN %');
            $sheet->mergeCells("I6:K6");
            $sheet->setCellValue('C7', 'REGULER');
            $sheet->setCellValue('D7', 'FREE PLU');
            $sheet->setCellValue('E7', 'TOTAL');
            $sheet->setCellValue('F7', 'REGULER');
            $sheet->setCellValue('G7', 'FREE PLU');
            $sheet->setCellValue('H7', 'TOTAL');
            $sheet->setCellValue('I7', 'REGULER');
            $sheet->setCellValue('J7', 'FREE PLU');
            $sheet->setCellValue('K7', 'TOTAL');

            $sheet->getStyle('A6:K7')->getAlignment()->setHorizontal('center');

            $highestRow = 8;
            foreach($stores as $row)
            {
                $values = [];
                $sheet->setCellValue('A'.$highestRow, $row->storename);

                foreach($days as $cell)
                {
                    if($row->storename === $cell->storename)
                    {
                        $sheet->setCellValue('B'.$highestRow, $cell->days);
                    }
                }

                foreach($data as $cell)
                {
                    if($row->storename === $cell->storename)
                    {
                        if($cell->pid != null)
                        {
                            $values[0][0] = $values[0][0] + $cell->sales;
                            $values[1][0] = $values[1][0] + $cell->margin;
                        }
                        else
                        {
                            $values[0][1] = $values[0][1] + $cell->sales;
                            $values[1][1] = $values[1][1] + $cell->margin;
                        }
                    }
                }

                for($i = 0;$i<2;$i++)
                {
                    for($j = 0; $j<2;$j++)
                    {
                        if($values[$i][$j] == null)
                        {
                            $values[$i][$j] = "0";
                        }
                    }
                }

                $sheet->setCellValue('C'.$highestRow, $values[0][0]);
                $sheet->setCellValue('D'.$highestRow, $values[0][1]);
                $sheet->setCellValue('E'.$highestRow, '=SUM(C'.$highestRow.':D'.$highestRow.')');
                $sheet->setCellValue('F'.$highestRow, $values[1][0]);
                $sheet->setCellValue('G'.$highestRow, $values[1][1]);
                $sheet->setCellValue('H'.$highestRow, '=SUM(F'.$highestRow.':G'.$highestRow.')');

                if($sheet->getCellByColumnAndRow(3, $highestRow)->getValue() != 0)
                {
                    $sheet->setCellValue('I'.$highestRow, '=(F'.$highestRow.'/C'.$highestRow.')*100');
                }
                else
                {
                    $sheet->setCellValue('I'.$highestRow, 0);
                }
                if($sheet->getCellByColumnAndRow(4, $highestRow)->getValue() != 0)
                {
                    $sheet->setCellValue('J'.$highestRow, '=(G'.$highestRow.'/D'.$highestRow.')*100');
                }
                else
                {
                    $sheet->setCellValue('J'.$highestRow, 0);
                }
                if($sheet->getCellByColumnAndRow(5, $highestRow)->getCalculatedValue() != 0)
                {
                    $sheet->setCellValue('K'.$highestRow, '=(H'.$highestRow.'/E'.$highestRow.')*100');
                }
                else
                {
                    $sheet->setCellValue('K'.$highestRow, 0);
                }
                $values = null;
                $highestRow +=1;
            }

            $lastColumn='L';
            for ($currentColumn = 'A'; $currentColumn != $lastColumn; $currentColumn++)
            {
                $sheet->getColumnDimension($currentColumn)->setAutoSize(true);
            }
            $sheet->getDefaultRowDimension()->setRowHeight(-1);

            $sheet->setTitle("rekap sales detail");

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="rekapsalesdetail.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
        }
        else if($type === "branchdate")
        {
            $branch = $request->get('efcabang');
            $store = $request->get('eftoko');
            $date = $request->get('efhari');

            if($date === "daterange")
            {
                $startdate = $request->get('startdate')." 00:00:00";
                $enddate = $request->get('enddate')." 23:59:59";

                $data = TDBBranch::ConnectToHeaderWithTmiType($branch,$store)
                    ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                    ->groupBy(\DB::raw('date(trx_headers.trx_date)'),'users.id')
                    ->get(['branches.name as branch','users.store_name as store','tmi_types.description as type',
                        \DB::raw("(select date(min(trx_headers.trx_date)) from branches join users on users.branch_id = branches.id join trx_headers on trx_headers.user_id = users.id where branches.id = 15 and users.store_name = store) as opendate"),
                        \DB::raw('date(trx_headers.trx_date) as tdate'),\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin'),\DB::raw('count(distinct trx_headers.id) as invcount'),\DB::raw('(sum(trx_headers.margin)/sum(trx_headers.grand_total))*100')])
                    ->toArray();
            }
            else
            {
                $data = TDBBranch::ConnectToHeaderWithTmiType($branch,$store)
                    ->where('trx_headers.trx_date','LIKE',$date)
                    ->groupBy(\DB::raw('date(trx_headers.trx_date)'),'users.id')
                    ->get(['branches.name as branch','users.store_name as store','tmi_types.description as type',
                        \DB::raw("(select date(min(trx_headers.trx_date)) from branches join users on users.branch_id = branches.id join trx_headers on trx_headers.user_id = users.id where branches.id = 15 and users.store_name = store) as opendate"),
                        \DB::raw('date(trx_headers.trx_date) as tdate'),\DB::raw('sum(trx_headers.grand_total) as sales'),\DB::raw('sum(trx_headers.margin) as margin'),\DB::raw('count(distinct trx_headers.id) as invcount'),\DB::raw('(sum(trx_headers.margin)/sum(trx_headers.grand_total))*100')])
                    ->toArray();
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'REKAP SPD, STRUK, APC TMI');
            $sheet->setCellValue('A2', 'PERIODE');
            $sheet->setCellValue('A3', 'IGR');

            $branchname = TDBBranch::where('id','=',$branch)->get(['name']);
            $sheet->setCellValue('B3', $branchname[0]->name);

            if($date === 'daterange')
            {
                $sheet->setCellValue('B2', $request->get('startdate').' s/d '.$request->get('enddate'));
            }
            else if($date === '%')
            {
                $sheet->setCellValue('B2', 'ALL TIME');
            }
            else
            {
                $sheet->setCellValue('B2', substr($date,0,4).' '.$months[(int)substr($date,5,2)]);
            }

            $sheet->setCellValue('A5', 'IGR');
            $sheet->setCellValue('B5', 'NAMA TMI');
            $sheet->setCellValue('C5', 'TYPE');
            $sheet->setCellValue('D5', 'TANGGAL GO');
            $sheet->setCellValue('E5', 'TANGGAL');
            $sheet->setCellValue('F5', 'SPD');
            $sheet->setCellValue('G5', 'STRUK');
            $sheet->setCellValue('H5', 'APC');
            $sheet->setCellValue('I5', 'MARGIN(%)');
            $sheet->getStyle('A5:I5')->getAlignment()->setHorizontal('center');

            $sheet->fromArray($data,NULL,'A6');

            $lastColumn='K';
            for ($currentColumn = 'A'; $currentColumn != $lastColumn; $currentColumn++)
            {
                $sheet->getColumnDimension($currentColumn)->setAutoSize(true);
            }
            $sheet->getDefaultRowDimension()->setRowHeight(-1);

            $sheet->setTitle("sales percab per date");

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="salespercabperdate.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
        }
        else
        {
            if($date === "daterange")
            {
                $startdate = $request->get('efstart')." 00:00:00";
                $enddate = $request->get('efend')." 23:59:59";

                $data = TDBBranch::ConnectToHeader($branch,$store,$cashier)
                    ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                    ->get(['branches.name as branch','users.store_name as store','operators.name as cashier','trx_headers.trx_no as invoice','trx_headers.trx_date as date','trx_headers.grand_total as total','trx_headers.margin as margin']);
            }
            else
            {
                $data = TDBBranch::ConnectToHeader($branch,$store,$cashier)
                    ->where('trx_headers.trx_date','LIKE',$date)
                    ->get(['branches.name as branch','users.store_name as store','operators.name as cashier','trx_headers.trx_no as invoice','trx_headers.trx_date as date','trx_headers.grand_total as total','trx_headers.margin as margin']);
            }

            $totalt = $data->sum('total');
            $totalm = $data->sum('margin');

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'CABANG');
            $sheet->setCellValue('B1', 'TOKO');
            $sheet->setCellValue('C1', 'KASIR');
            $sheet->setCellValue('D1', 'INVOICE');
            $sheet->setCellValue('E1', 'TANGGAL');
            $sheet->setCellValue('F1', 'TOTAL');
            $sheet->setCellValue('G1', 'MARGIN');
            $sheet->getStyle('A1:G1')->getAlignment()->setHorizontal('center');

            $no = 2;
            foreach($data as $row)
            {
                $sheet->setCellValue('A'.$no, $row->branch);
                $sheet->setCellValue('B'.$no, $row->store);
                $sheet->setCellValue('C'.$no, $row->cashier);
                $sheet->setCellValue('D'.$no, $row->invoice);
                $sheet->setCellValue('E'.$no, $row->date);
                $sheet->setCellValue('F'.$no, $row->total);
                $sheet->setCellValue('G'.$no, $row->margin);
                $no++;
            }

            $highestRow = $sheet->getHighestRow()+1;

            $sheet->setCellValue('F'.$highestRow, $totalt);
            $sheet->setCellValue('G'.$highestRow, $totalm);

            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setAutoSize(true);
            $sheet->getColumnDimension('E')->setAutoSize(true);
            $sheet->getColumnDimension('F')->setAutoSize(true);
            $sheet->getColumnDimension('G')->setAutoSize(true);
            $sheet->getDefaultRowDimension()->setRowHeight(-1);

            $sheet->setTitle("sales");

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="sales.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
        }
    }

    public function exportPareto(Request $request)
    {
        $branch = $request->get('efcabang');
        $store = $request->get('eftoko');
        $type = $request->get('eftipe');
        $date = $request->get('efhari');

        if($type === "cnt")
        {
            $arr = ['branches.name as branch','users.store_name as store','user_products.description as product',\DB::raw('count(distinct(trx_details.trx_header_id)) as total')];
        }
        else if($type === "qty")
        {
            $arr = ['branches.name as branch','users.store_name as store','user_products.description as product',\DB::raw('sum(trx_details.qty) as total')];
        }
        else if($type === "prc")
        {
            $arr = ['branches.name as branch','users.store_name as store','user_products.description as product',\DB::raw('sum(trx_details.price*trx_details.qty) as total')];
        }

        if($date === "daterange")
        {
            $startdate = $request->get('startdate')." 00:00:00";
            $enddate = $request->get('enddate')." 23:59:59";

            $data = TDBBranch::ConnectToDetail($branch,$store)
                ->whereBetween('trx_headers.trx_date',[$startdate,$enddate])
                ->groupBy('user_products.id')
                ->orderBy('total','desc')
                ->get($arr);
        }
        else
        {
            $data = TDBBranch::ConnectToDetail($branch,$store)
                ->where('trx_headers.trx_date','LIKE',$date)
                ->groupBy('user_products.id')
                ->orderBy('total','desc')
                ->get($arr);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'CABANG');
        $sheet->setCellValue('B1', 'TOKO');
        $sheet->setCellValue('C1', 'PRODUK');
        $sheet->setCellValue('D1', 'TOTAL');
        $sheet->getStyle('A1:D1')->getAlignment()->setHorizontal('center');

        $no = 2;
        foreach($data as $row)
        {
            $sheet->setCellValue('A'.$no, $row->branch);
            $sheet->setCellValue('B'.$no, $row->store);
            $sheet->setCellValue('C'.$no, $row->product);
            $sheet->setCellValue('D'.$no, $row->total);
            $no++;
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getDefaultRowDimension()->setRowHeight(-1);

        $sheet->setTitle("pareto");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="pareto.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function exportPb(Request $request)
    {
        $branch = $request->get('efcabang');
        $store = $request->get('eftoko');
        $date = $request->get('efhari');

        if($date === "daterange")
        {
            $startdate = $request->get('start');
            $enddate = $request->get('end');

            $data = TDBBranch::ConnectToPb($branch,$store)
                ->whereBetween('pb_headers.po_date',[$startdate,$enddate])
                ->get(['pb_headers.id as id','branches.name as branch','users.member_code as member_code','users.store_name as store','pb_headers.po_number as po_no','pb_headers.po_date as po_date','pb_headers.name as name','pb_headers.email as email','pb_headers.phone_number as phone','pb_headers.address as address','pb_headers.qty_order as qtyo','pb_headers.qty_fulfilled as qtyf','pb_headers.price_order as priceo','pb_headers.price_fulfilled as pricef','pb_headers.flag_free_delivery as isfree','pb_statuses.title as status','pb_headers.flag_sent as issent']);
        }
        else
        {
            $data = TDBBranch::ConnectToPb($branch,$store)
                ->where('pb_headers.po_date','LIKE',$date)
                ->get(['pb_headers.id as id','branches.name as branch','users.member_code as member_code','users.store_name as store','pb_headers.po_number as po_no','pb_headers.po_date as po_date','pb_headers.name as name','pb_headers.email as email','pb_headers.phone_number as phone','pb_headers.address as address','pb_headers.qty_order as qtyo','pb_headers.qty_fulfilled as qtyf','pb_headers.price_order as priceo','pb_headers.price_fulfilled as pricef','pb_headers.flag_free_delivery as isfree','pb_statuses.title as status','pb_headers.flag_sent as issent']);
        }

        $mindate = $data->min('po_date');
        $maxdate = $data->max('po_date');

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'LAPORAN DAN REALISASI PB');
        $sheet->mergeCells("A1:G1");

        $sheet->setCellValue('A2', 'PERIODE '.$mindate.' s/d '.$maxdate);
        $sheet->mergeCells("A2:G2");
        $sheet->getStyle('A1:A2')->getAlignment()->setHorizontal('center');

        $highestRow = 4;
        $no = 1;
        foreach($data as $row)
        {
            $sheet->setCellValue('A'.$highestRow, 'No. PO');
            $sheet->setCellValue('A'.($highestRow+1), 'Tanggal Pesan');
            $sheet->setCellValue('A'.($highestRow+2), 'Kode Member');
            $sheet->setCellValue('A'.($highestRow+3), 'Nama Toko');
            $sheet->setCellValue('A'.($highestRow+4), 'Pemesan');
            $sheet->setCellValue('A'.($highestRow+5), 'Email');
            $sheet->setCellValue('A'.($highestRow+6), 'Alamat');
            $sheet->setCellValue('A'.($highestRow+7), 'No. Telp');
            $sheet->setCellValue('A'.($highestRow+8), 'Gratis Biaya Pengiriman');
            $sheet->setCellValue('A'.($highestRow+9), 'Status');
            $sheet->setCellValue('A'.($highestRow+10), 'Terkirim');
            $sheet->getStyle("A".$highestRow.":A".($highestRow+10))->getFont()->setBold( true );

            if($row->isfree == 1){
                $isfree = 'Gratis';
            }
            else
            {
                $isfree = 'Tidak';
            }

            if($row->issent == 1){
                $issent = 'Terkirim';
            }
            else
            {
                $issent = 'Belum Terkirim';
            }

            $sheet->setCellValue('B'.$highestRow, $row->po_no);
            $sheet->setCellValue('B'.($highestRow+1), $row->po_date);
            $sheet->setCellValue('B'.($highestRow+2), $row->member_code);
            $sheet->setCellValue('B'.($highestRow+3), $row->store);
            $sheet->setCellValue('B'.($highestRow+4), $row->name);
            $sheet->setCellValue('B'.($highestRow+5), $row->email);
            $sheet->setCellValue('B'.($highestRow+6), $row->address);
            $sheet->setCellValue('B'.($highestRow+7), $row->phone);
            $sheet->setCellValue('B'.($highestRow+8), $isfree);
            $sheet->setCellValue('B'.($highestRow+9), $row->status);
            $sheet->setCellValue('B'.($highestRow+10), $issent);

            $highestRow = $sheet->getHighestRow()+1;
            $sheet->setCellValue('A'.$highestRow, 'No.');
            $sheet->setCellValue('B'.$highestRow, 'PLU');
            $sheet->setCellValue('C'.$highestRow, 'Nama Produk');
            $sheet->setCellValue('D'.$highestRow, 'Jumlah Realisasi');
            $sheet->setCellValue('E'.$highestRow, 'Jumlah Pesanan');
            $sheet->setCellValue('F'.$highestRow, 'Harga Realisasi');
            $sheet->setCellValue('G'.$highestRow, 'Harga Pesanan');

            $sheet->getStyle("A".$highestRow.":G".$highestRow)->getFont()->setBold( true );
            $highestRow = $sheet->getHighestRow()+1;

            $details = TDBPbDetail::with('products')->where('pb_header_id','=',$row->id)->get();
            foreach($details as $detail)
            {
                $sheet->setCellValue('A'.$highestRow, $no);
                $sheet->getStyle('A'.$highestRow)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                $sheet->setCellValue('B'.$highestRow, $detail->products->plu);
                $sheet->getStyle('B'.$highestRow)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                $sheet->setCellValue('C'.$highestRow, $detail->products->description);
                $sheet->setCellValue('D'.$highestRow, $detail->qty_fulfilled);
                $sheet->setCellValue('E'.$highestRow, $detail->qty_order);
                $sheet->setCellValue('F'.$highestRow, $detail->price_fulfilled);
                $sheet->setCellValue('G'.$highestRow, $detail->price_order);
                $highestRow++;
                $no++;
            }

            $highestRow = $sheet->getHighestRow()+2;
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getDefaultRowDimension()->setRowHeight(-1);

        $sheet->setTitle("pb");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="pb.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function exportArchive(Request $request)
    {
        $branch = $request->get('branch');
        $store = $request->get('store');

        $data = TDBBranch::ConnectToUserProducts($branch,$store)
            ->whereNotNull('user_products.deleted_at')
            ->get(['branches.name as branch','users.store_name as store','user_products.plu as plu','user_products.description as desc','user_products.unit as unit','user_products.fraction as frac','user_products.deleted_at as date']);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'LAPORAN ARSIP BARANG');
        $sheet->mergeCells("A1:F1");
        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:F1')->getFont()->setBold( true );

        $highestRow = 3;
        $no = 1;
        foreach($data->unique('store') as $row)
        {
            $storetemp = $row->store;
            $sheet->setCellValue('A'.$highestRow, 'Cabang');
            $sheet->setCellValue('A'.($highestRow+1), 'Toko');
            $sheet->setCellValue('B'.$highestRow, $row->branch);
            $sheet->setCellValue('B'.($highestRow+1), $row->store);
            $sheet->getStyle('A'.$highestRow.':A'.($highestRow+1))->getFont()->setBold( true );

            $sheet->setCellValue('A'.($highestRow+2), 'No.');
            $sheet->setCellValue('B'.($highestRow+2), 'PLU');
            $sheet->setCellValue('C'.($highestRow+2), 'Deskripsi');
            $sheet->setCellValue('D'.($highestRow+2), 'Satuan');
            $sheet->setCellValue('E'.($highestRow+2), 'Fraksi');
            $sheet->setCellValue('F'.($highestRow+2), 'Tanggal Diarsipkan');
            $sheet->getStyle('A'.($highestRow+2).':F'.($highestRow+2))->getFont()->setBold( true );

            $highestRow = $sheet->getHighestRow()+1;
            foreach($data as $detail)
            {
                if($detail->store === $storetemp)
                {
                    $sheet->setCellValue('A'.$highestRow, $no);
                    $sheet->setCellValue('B'.$highestRow, $detail->plu);
                    $sheet->getStyle('B'.$highestRow)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                    $sheet->setCellValue('C'.$highestRow, $detail->desc);
                    $sheet->setCellValue('D'.$highestRow, $detail->unit);
                    $sheet->setCellValue('E'.$highestRow, $detail->frac);
                    $sheet->setCellValue('F'.$highestRow, $detail->date);
                    $no++;
                    $highestRow++;
                }
            }
            $highestRow = $sheet->getHighestRow()+2;
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getDefaultRowDimension()->setRowHeight(-1);

        $sheet->setTitle("arsip");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="arsip.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function exportStock(Request $request)
    {
        //prep memory for large amount of data
        ini_set('memory_limit', '218M');
        $branch = $request->get('efcabang');
        $store = $request->get('eftoko');

        $data = TDBBranch::ConnectUserProductToDiv($branch,$store)
            ->whereNull('user_products.deleted_at')
            ->get(['categories.category as category','user_products.plu as plu','user_products.id as pid','user_products.description as desc',
                \DB::raw('(select sum(trx_details.qty)/30
                    from users
                    join user_products on users.id = user_products.user_id
                    join trx_details on user_products.id = trx_details.user_product_id
                    join trx_headers on trx_headers.id = trx_details.trx_header_id
                    where users.id = '.$store.' and user_products.id = pid and date(trx_headers.trx_date) between (curdate() - interval 30 day) and curdate()) as avgsales'),
                'user_products.stock as stock','user_products.min_stock as minq','user_products.max_stock as maxq','user_products.deleted_at as date']);

        $div=[];
        $dep=[];
        $kat=[];
        foreach($data as $row => $item)
        {
            $temp = Margin::join('divisi','master_margin.div','=','divisi.DIV_KODEDIVISI')
                ->join('department', function ($join) {
                    $join->on('master_margin.dep','=','department.DEP_KODEDEPARTEMENT');
                    $join->on('divisi.DIV_KODEDIVISI','=','department.DEP_KODEDIVISI');
                })
                ->join('category', function ($join) {
                    $join->on('master_margin.kat','=','category.KAT_KODEKATEGORI');
                    $join->on('department.DEP_KODEDEPARTEMENT','=','category.KAT_KODEDEPARTEMENT');
                })
                ->where('master_margin.id','=',$item->category)
                ->get(['divisi.DIV_KODEDIVISI as div','department.DEP_KODEDEPARTEMENT as dep','category.KAT_KODEKATEGORI as kat']);
            $div[] = $temp[0]->div;
            $dep[] = $temp[0]->dep;
            $kat[] = $temp[0]->kat;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'LAPORAN STOK BARANG');
        $sheet->mergeCells("A1:F1");
        $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:F1')->getFont()->setBold( true );

        $highestRow = 3;
        $no = 0;
        foreach($data->unique('store') as $row)
        {
            $storetemp = $row->store;
            $sheet->setCellValue('A'.$highestRow, 'Cabang');
            $sheet->setCellValue('A'.($highestRow+1), 'Toko');
            $sheet->setCellValue('B'.$highestRow, $row->branch);
            $sheet->setCellValue('B'.($highestRow+1), $row->store);
            $sheet->getStyle('A'.$highestRow.':A'.($highestRow+1))->getFont()->setBold( true );

            $sheet->setCellValue('A'.($highestRow+2), 'DIV');
            $sheet->setCellValue('B'.($highestRow+2), 'DEPT');
            $sheet->setCellValue('C'.($highestRow+2), 'KAT');
            $sheet->setCellValue('D'.($highestRow+2), 'PLU');
            $sheet->setCellValue('E'.($highestRow+2), 'DESKRIPSI');
            $sheet->setCellValue('F'.($highestRow+2), 'STOK QTY');
            $sheet->setCellValue('G'.($highestRow+2), 'AVG SALES');
            $sheet->setCellValue('H'.($highestRow+2), 'MIN DIS');
            $sheet->setCellValue('I'.($highestRow+2), 'MIN QTY');
            $sheet->setCellValue('J'.($highestRow+2), 'MAX QTY');
            $sheet->getStyle('A'.($highestRow+2).':J'.($highestRow+2))->getFont()->setBold( true );

            $highestRow = $sheet->getHighestRow()+1;
            foreach($data as $detail)
            {
                if($detail->store === $storetemp)
                {
                    $sheet->setCellValue('A'.$highestRow, $div[$no]);
                    $sheet->setCellValue('B'.$highestRow, $dep[$no]);
                    $sheet->getStyle('B'.$highestRow)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                    $sheet->setCellValue('C'.$highestRow, $kat[$no]);
                    $sheet->getStyle('C'.$highestRow)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                    $sheet->setCellValue('D'.$highestRow, $row->plu);
                    $sheet->getStyle('D'.$highestRow)->getNumberFormat()->setFormatCode( \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT );
                    $sheet->setCellValue('E'.$highestRow, $detail->desc);
                    $sheet->setCellValue('F'.$highestRow, $detail->stock);
                    if($detail->avgsales != null)
                    {
                        $sheet->setCellValue('G'.$highestRow, $detail->avgsales);
                    }
                    else
                    {
                        $sheet->setCellValue('G'.$highestRow, '0');
                    }
                    $sheet->setCellValue('H'.$highestRow, 'MIN DIS');
                    $sheet->setCellValue('I'.$highestRow, $detail->minq);
                    $sheet->setCellValue('J'.$highestRow, $detail->maxq);
                    $no++;
                    $highestRow++;
                }
            }
            $highestRow = $sheet->getHighestRow()+2;
        }

        $lastColumn='K';
        for ($currentColumn = 'A'; $currentColumn != $lastColumn; $currentColumn++)
        {
            $sheet->getColumnDimension($currentColumn)->setAutoSize(true);
        }
        $sheet->getDefaultRowDimension()->setRowHeight(-1);

        $sheet->setTitle("stok");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="stok.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function deletePlu(Request $Request){
        $toBeDeleted = MasterPlu::find($Request->get('id'));
        $toBeDeleted->delete();

        return redirect('listmember');
    }

    public function getDevAccessToken(Request $request)
    {
        $ch = curl_init(config('urls.basedevapi').config('urls.devloginapi'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Connection: keep-alive'
        ));
        $array = ['client_id' => 1, 'client_secret' => 'VPbpBVpO8wnIlhdTP7s6OidAqzyzltCQqyhq1MN6'];

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    public function getDevAllVersion(Request $request)
    {
        $ch = curl_init(config('urls.basedevapi').config('urls.devgetallapi'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: '.$request->get('auth'),
            'Connection: keep-alive'
        ));

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    public function devUpdateVersion(Request $request)
    {
        $ch = curl_init(config('urls.basedevapi').config('urls.devupdateapi'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: '.$request->get('auth'),
            'Connection: keep-alive'
        ));
        $array = ['server_id' => $request->get('id'), 'version_name' => $request->get('ver'), 'description' => $request->get('desc')];

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    public function devInsertVersion(Request $request)
    {
        $ch = curl_init(config('urls.basedevapi').config('urls.devinsertapi'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: '.$request->get('auth'),
            'Connection: keep-alive'
        ));
        $array = ['version_name' => $request->get('ver'), 'description' => $request->get('desc')];

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    public function getMemberPLU(Request $Request){

        if ($Request->get('tipemember') == "x") {
            $tipe = "%";
        }else{
            $tipe = $Request->get('tipemember');
        }

        $MemberPLUTmiAssoc = MasterPlu::getPlu($tipe);

        foreach ($MemberPLUTmiAssoc as $row) {
//            $row->aksi  = '<button id="btn_edit_plu" type="button" class="btn btn-info flat" style="width:70px;margin-bottom: 5px;" value="' . $row->id . '"> Edit </button>';
        }

        return \Datatables::of($MemberPLUTmiAssoc)->make(true);

    }



    public function getPluAjax(Request $Request){

        $id = $Request->id;

        $data = \DB::table('master_plu')
            ->leftJoin('master_toko', 'master_toko.id', '=', 'master_plu.tmi_id')
            ->Where('master_plu.id', $id)
            ->Get();
        return $data;
    }





    public function PostMasterMargin(Request $request){

        $splu = $request->splu;
        $cabpecah = explode(',',$splu);

        $branch = \DB::table('branches')
            ->Selectraw('kode_igr')
            ->whereNotIn('kode_igr', $cabpecah)
            ->get();

        $date = Carbon::Now();

        $newMrg = new Margin();
        $newMrg->kode_tmi = $request->tipe;
        $newMrg->kode_mrg =  $request->tipe. $request->dep. $request->kat;
        if(count($cabpecah) == 21){
            $newMrg->flag_cab = 1;
        }else{
            $newMrg->flag_cab = 0;
        }

        $newMrg->div = $request->div;
        $newMrg->dep = $request->dep;
        $newMrg->kat = $request->kat;
        $newMrg->margin_min = $request->min;
        $newMrg->margin_max = $request->max;
        $newMrg->margin_saran = $request->saran;
        $newMrg->created_at = Carbon::now();
        $newMrg->modify_at = Carbon::now();
        $newMrg->created_by = 'ADM';
        $newMrg->modify_by = 'ADM';
        $newMrg->save();

        foreach($cabpecah as $sp) {
            $newMrgDtl = new MarginDetail();
            $newMrgDtl->margin_id = $newMrg->id;
            $newMrgDtl->kode_igr = $sp;
            $newMrgDtl->created_at = $date;
            $newMrgDtl->updated_at = $date;
            $newMrgDtl->deleted_at = null;
            $newMrgDtl->modify_by = 'ADM';;
            $newMrgDtl->save();
        }

        return "true";
    }




    public function getSearchPlu(Request $Request){

        $kodeplu = $Request->id;
        $kodeigr = $Request->cab;


        $prodmastsearch = Margin::Distinct()
            ->Join('products', function ($join) {
                $join->on('master_margin.div', '=', 'products.kode_division');
                $join->on('master_margin.dep', '=', 'products.kode_department');
                $join->on('master_margin.kat', '=', 'products.kode_category');
            })
            ->leftJoin('category', function ($join) {
                $join->on('products.kode_department', '=', 'category.kat_kodedepartement');
                $join->on('products.kode_category', '=', 'category.kat_kodekategori');
            })
            ->Where('prdcd', 'LIKE', '%' . $kodeplu . '%')
            ->groupby('prdcd')
            ->get();


        return $prodmastsearch;
    }


    public function getCabPlu(Request $Request){

        $kodeplu = $Request->id;
//        $kodeigr = $Request->cab;

        $prodmastcab = \DB::connection('webmm')->table('products')
            ->Select('name')
            ->join('branches', 'products.kode_igr', '=', 'branches.kode_igr')
//            ->Where('prdcd', $kodeplu)
            ->Where('prdcd', 'LIKE', '%' . $kodeplu . '%')
//            ->groupby('prdcd')
            ->get();

        return $prodmastcab;
    }



    public function EditPluAjax(Request $request){
        $id = $request->id;

        $data = MasterPlu::find($id);
        $data->hrg_jualigr = $request->hrg_jualigr;
        $data->save();

        return "true";
    }


    public function AktivasiMember(Request $request){
        $id = $request->tipeid;

        $data = MasterToko::find($id);

        $data->tmi_id = $request->tipetmi;
        $data->status = 1;
        $data->save();

        return "true";
    }


    public function AddPluTmi(request $request)
    {
        ini_set('memory_limit', '-1');

        ini_set('max_execution_time', 300);

        $cekisi = "";
        $branchFormat = "";

        $date = Carbon::Now();

        $path = $request->file('import_file')->getRealPath();

        $branch = \DB::table('branches')
            ->get();

        foreach($branch as $index => $row) {
            $branchFormat .= "<option style='font-size: 12px;' value='" . $row->kode_igr . "'>" . $row->kode_igr . " --> " . $row->name . "</option>";
        }

        $plu = Excel::load($path)->calculate()->toArray();


        \DB::beginTransaction();
        try{
//        if (!empty($plu)) {
            $tempErrorFiles = array();
            foreach ($plu as $key => $value) {
//                    if ($value['kodemgn'] != null || $value['kodemgn'] != "") {
//                        $kode = \DB::table('margin_details')
//                            ->Selectraw('kode_mrg, kode_igr')
//                            ->Join('master_margin', 'margin_details.margin_id', '=', 'master_margin.id')
//                            ->Where('kode_mrg', $value['kodemgn'])
//                            ->WhereNull('deleted_at')
//                            ->get();
//                    }
//                    if($value['kodemgn'] != null || $value['kodemgn'] != ""){
                $PLU = substr($value['plu'], 0, 6);

                $product = \DB::table('products')
                    ->Selectraw('prdcd, kode_igr, hrg_jual, frac, unit, long_description, kode_tag, min_jual')
                    ->Where('prdcd', 'LIKE', '' . $PLU . '%')
                    ->get();

                $countplu = \DB::table('master_plu')
                    ->Where('kodeplu', 'LIKE', '' . $PLU . '%')
                    ->Where('mrg_id', $value['kodemgn'])
                    ->get();

                $countmrg = \DB::table('master_margin')
                    ->Where('kode_mrg', $value['kodemgn'])
                    ->get();

//                    dd(count($countplu));

                if ($product != null && count($countplu) == 0 && count($countmrg) > 0) {
                    foreach ($product as $key => $row) {
                        $plutmi = New MasterPlu;
                        $plutmi->kodeplu = $row->prdcd;
                        $plutmi->kode_igr = $row->kode_igr;
                        $plutmi->hrg_jual = $row->hrg_jual;
                        $plutmi->display = $value['display'];
                        $plutmi->unit_tmi = $value['satjualtmi'];
                        $plutmi->mrg_id = $value['kodemgn'];
                        $plutmi->frac_tmi = $value['konversi']*$row->frac;
                        $plutmi->qty_min = $value['minqty'];
                        $plutmi->qty_max = $value['maxqty'];
                        $plutmi->min_dis = $value['mindis'];
                        $plutmi->min_jual = $row->min_jual;
                        $plutmi->frac_igr = $row->frac;
                        $plutmi->unit_igr = $row->unit;
                        $plutmi->tag = $row->kode_tag;
                        $plutmi->long_desc = $row->long_description;
//                            $plutmi->barcode = $row->brc_barcode;
                        $plutmi->save();
                    }

                } else {
                    array_push($tempErrorFiles, $value['plu']);
                }

            }

//            dd($tempErrorFiles);

            if (count($tempErrorFiles) > 0) {
                $message = '<br/>Kode PLU :<br/>';
                foreach ($tempErrorFiles as $file)
                    $message = $message . ' ' . $file . ', ';
                $message = $message . '<br/> gagal di-Upload! Format atau Nama Kode Sudah Ada atau Tidak Terdaftar';
                $request->session()->flash('error_message', $message);
            } else {
                $request->session()->flash('success_message', 'Data berhasil di-Upload!');
            }

            \DB::commit();
            return view('admin.uploadplu')->with('Tes', 'Berhasil Upload');

        }catch(Exception $ex){
            \DB::rollBack();
            return view('admin.uploadplu')->with('nodata', 'Gagal Upload File');
        }


//            return view('admin.uploadplu')->with('branch',$branchFormat);
//        }
//        return view('admin.uploadplu')->with('nodata', 'Tidak Ada Data yang Diupload')->with('getmember', $this->MemberView())->with('getmember', $this->MemberView());

    }

    public function UploadPlu(request $request)
    {
        ini_set('memory_limit', '-1');

        ini_set('max_execution_time', 300);

        $cekisi = "";
        $branchFormat = "";

        $date = Carbon::Now();

        $path = $request->file('import_file')->getRealPath();

        $branch = \DB::table('branches')
            ->get();

        foreach($branch as $index => $row) {
            $branchFormat .= "<option style='font-size: 12px;' value='" . $row->kode_igr . "'>" . $row->kode_igr . " --> " . $row->name . "</option>";
        }

        $plu = Excel::load($path)->calculate()->toArray();


        \DB::beginTransaction();
        try{

//            $tempErrorFiles = array();
            foreach ($plu as $key => $value) {
                $PLU = substr($value['plu'], 0, 6);

                $product = \DB::table('products')
                    ->Selectraw('prdcd, kode_igr, hrg_jual, frac, unit, long_description, kode_tag, min_jual')
                    ->Where('prdcd', 'LIKE', '' . $PLU . '%')
                    ->get();
//                $countplu = \DB::table('master_plu')
//                    ->Where('kodeplu', 'LIKE', '' . $PLU . '%')
//                    ->Where('mrg_id', $value['kodemgn'])
//                    ->get();
//
//                $countmrg = \DB::table('master_margin')
//                    ->Where('kode_mrg', $value['kodemgn'])
//                    ->get();

//                    dd(count($countplu));

//                if ($product != null && count($countplu) == 0 && count($countmrg) > 0) {
                foreach ($product as $key => $row) {
                    $plutmi = New MasterPlu;
                    $plutmi = MasterPlu::firstOrNew(['mrg_id' => $value['kodemgn'], 'kodeplu' => $row->prdcd, 'kode_igr' => $row->kode_igr]);
//                        $plutmi->kodeplu = $row->prdcd;
                    $plutmi->kode_igr = $row->kode_igr;
                    $plutmi->hrg_jual = $row->hrg_jual;
                    $plutmi->display = $value['display'];
                    $plutmi->unit_tmi = $value['satjualtmi'];
//                        $plutmi->mrg_id = $value['kodemgn'];
                    $plutmi->frac_tmi = $value['konversi']*$row->frac;
                    $plutmi->qty_min = $value['minqty'];
                    $plutmi->qty_max = $value['maxqty'];
                    $plutmi->min_dis = $value['mindis'];
                    $plutmi->min_jual = $row->min_jual;
                    $plutmi->frac_igr = $row->frac;
                    $plutmi->unit_igr = $row->unit;
                    $plutmi->tag = $row->kode_tag;
                    $plutmi->long_desc = $row->long_description;
//                            $plutmi->barcode = $row->brc_barcode;
                    $plutmi->save();
                }

//                } else {
//                    array_push($tempErrorFiles, $value['plu']);
//                }

            }




//            dd($tempErrorFiles);

//            if (count($tempErrorFiles) > 0) {
//                $message = '<br/>Kode PLU :<br/>';
//                foreach ($tempErrorFiles as $file)
//                    $message = $message . ' ' . $file . ', ';
//                $message = $message . '<br/> gagal di-Upload! Format atau Nama Kode Sudah Ada atau Tidak Terdaftar';
//                $request->session()->flash('error_message', $message);
//            } else {
//                $request->session()->flash('success_message', 'Data berhasil di-Upload!');
//            }

            \DB::commit();
            $request->session()->flash('success_message', 'Data berhasil di-Upload!');
            return view('admin.uploadplu')->with('suc', 'Berhasil Upload');

        }catch(Exception $ex){
            \DB::rollBack();
            return view('admin.uploadplu')->with('nodata', 'Gagal Upload File');
        }


//            return view('admin.uploadplu')->with('branch',$branchFormat);
//        }
//        return view('admin.uploadplu')->with('nodata', 'Tidak Ada Data yang Diupload')->with('getmember', $this->MemberView())->with('getmember', $this->MemberView());

    }


    public function PostUploadMargin(request $request)
    {
        $cekisi = "";

        $date = Carbon::Now();

        $path = $request->file('import_file')->getRealPath();

        $margin = Excel::load($path)->calculate()->toArray();

        $branch = \DB::table('branches')
            ->get();

//            \DB::beginTransaction();
//            try{
//                $tempErrorFiles = array();
        foreach ($margin as $key => $value) {

            $cekMgr = \DB::table('master_margin')
                ->Where('kode_mrg', $value['kodemgn'])
                ->Pluck('kode_mrg');

//                    if (count($cekMgr) < 1) {

            $newmaster = Margin::firstOrNew(['kode_tmi' => $value['kodetmi'], 'kode_mrg' => $value['kodemgn']]);
            $newmaster->margin_saran = $value['saran'];
//                        $newmaster->margin_max = $value['max'];
            $newmaster->margin_min = $value['min'];
            $newmaster->div = $value['div'];
            $newmaster->dep = $value['dept'];
            $newmaster->kat = $value['kat'];
            $newmaster->flag_cab = 1;
            $newmaster->created_by = 'ADM';
            $newmaster->created_at = $date;
            $newmaster->modify_at = $date;
            $newmaster->modify_by = 'ADM';
            $newmaster->save();

            if (count($cekMgr) < 1) {
                foreach ($branch as $key => $row) {
                    $dtl = New MarginDetail();
//                            $dtl = MarginDetail::firstOrNew(['margin_id' => $newmaster->id, 'kode_igr' => $row->kode_igr]);
                    $dtl->margin_id = $newmaster->id;
                    $dtl->kode_igr = $row->kode_igr;
                    $dtl->created_at = $date;
                    $dtl->updated_at = $date;
                    $dtl->modify_by = 'ADM';
                    $dtl->save();
                }
            }
//                    } else {
//                        array_push($tempErrorFiles, $cekMgr);
//                    }
//
        }

//                if (count($tempErrorFiles) > 0) {
//                    $message = '<br/>Kode Master :<br/>';
//                    foreach ($tempErrorFiles as $file)
//                    $message = $message . ' ' . $file . ', ';
//                    $message = $message . ' gagal di-Upload! Format atau Nama Kode Sudah Ada';
//                    $request->session()->flash('error_message', $message);
//                } else {
//                    $request->session()->flash('success_message', 'Data berhasil di-Upload!');
//                }


//                \DB::commit();
        return view('admin.uploadmargin')->with('Tes', 'Berhasil Upload');

//            }catch(Exception $ex){
//                \DB::rollBack();
//                return view('admin.uploadmargin')->with('nodata', 'Gagal Upload File');
//            }

//        }
//        return view('admin.uploadmargin')->with('nodata', 'Tidak Ada Data yang Diupload');
    }



    public function getMarginAjax(Request $request){
        $id = $request->id;

        $data1 = Margin::SelectRaw('flag_cab, tipe_tmi.nama as tipetmi, margin_min, margin_max, margin_saran, kat_namakategori, master_margin.id as idmrg')
            ->leftJoin('tipe_tmi', 'master_margin.kode_tmi', '=', 'tipe_tmi.kode_tmi')
//            ->leftJoin('branches', 'master_margin.kode_igr', '=', 'branches.kode_igr')
            ->leftJoin('divisi', 'div_kodedivisi', '=', 'div')
//            ->Join('department', 'dep_kodedepartement', '=', 'dep')
            ->leftJoin('department', function ($join) {
                $join->on('master_margin.dep', '=', 'department.dep_kodedepartement');
                $join->on('divisi.div_kodedivisi', '=', 'department.dep_kodedivisi');
            })
            ->leftJoin('category', function ($join) {
                $join->on('master_margin.kat', '=', 'category.kat_kodekategori');
                $join->on('department.dep_kodedepartement', '=', 'category.kat_kodedepartement');
            })
            ->Where('master_margin.id', $id)
            ->OrderBy('master_margin.id', 'DESC')
//            ->Join('category', 'kat_kodekategori', '=', 'kat')
            ->get();

        $data2 = MarginDetail::Selectraw('margin_id, kode_igr')
            ->Where('margin_id',$id)
            ->WhereNull('deleted_at')
            ->get();

//        return $data1;

        return $data1.'!@#$'.$data2;
    }


    public function EditMargin(Request $request){
        $id = $request->id;
        $splu = $request->splu;
        $cabpecah = explode(',',$splu);

//        $cekisibranch = MarginDetail::Selectraw('margin_id, kode_igr')
//            ->WhereNotIn('margin_id',$cabpecah)
//            ->get();

        $cekisinotbranch = \DB::table('margin_details')
            ->Selectraw('kode_igr, margin_id')
            ->whereNotIn('kode_igr', $cabpecah)
            ->Where('margin_id', $id)
            ->get();

        $cekisibranch = \DB::table('margin_details')
            ->Selectraw('kode_igr, margin_id')
            ->whereIn('kode_igr', $cabpecah)
            ->Where('margin_id', $id)
            ->get();


        $date = Carbon::Now();


        if(count($request->splu) > 0){

            foreach($cekisinotbranch as $sp) {
                \DB::table('margin_details')
                    ->where('margin_id', $sp->margin_id)
                    ->where('kode_igr', $sp->kode_igr)
                    ->update(['deleted_at' => $date]);
            }

            foreach($cekisibranch as $sp) {
                \DB::table('margin_details')
                    ->where('margin_id', $sp->margin_id)
                    ->where('kode_igr', $sp->kode_igr)
                    ->update(['deleted_at' => null]);
            }

            $data = Margin::find($id);
            $data->margin_min = $request->minx;
            $data->margin_max = $request->maxx;
            $data->margin_saran = $request->saranx;
            if(count($cabpecah) < 21){
                $data->flag_cab = 0;
            }else{
                $data->flag_cab = 1;
            }
            $data->save();

        }else{

            $data = Margin::find($id);
            $data->margin_min = $request->minx;
            $data->margin_max = $request->maxx;
            $data->margin_saran = $request->saranx;
            $data->save();
        }

        return "true";
    }




}
