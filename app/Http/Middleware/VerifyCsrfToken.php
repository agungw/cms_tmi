<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'pluAjax',
        'editplu',
        'getlogin',
        'searchPlu',
        'memberAjax',
        'editmember',
        'aktivasimember',
        'registermember',
        'addmastermargin',
        'MarginAjax',
        'editmargin',
        'getprodmast',
        'uploadmastermargin',
        'gettipetmi',
        'authenticate' 
    ];
}
