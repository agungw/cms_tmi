<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
// Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
// error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

Route::get('login', 'LoginController@index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', function () {
    Auth::logout();
    return redirect('login');
});

Route::post('getlogin', 'LoginController@getLoginWeb');

Route::group(['middleware' => 'auth'], function(){

    //Register Member
    Route::get('inputmember', 'MemberController@getMemberTmi');
//    Route::post('addmember', 'ProjectController@postAddMember');
    Route::get('admin/member/datatable', 'MemberController@getMemberDatatables');
    Route::get('get_member', 'MemberController@get_member');
    Route::post('registermember', 'MemberController@getRegisterMember');
    Route::post('editmember', 'MemberController@EditMember');
    Route::post('memberAjax', 'MemberController@getMemberAjax');


    Route::get('uploadplutmi', 'ProjectController@getViewMember');

    Route::get('admin/plu/datatable', 'ProjectController@getMemberPLU');

    Route::post('uploadplu', 'ProjectController@UploadPlu');

    Route::get('listmember', 'ProjectController@getListMember');

    Route::get('deleteplu', 'ProjectController@deletePlu');

    Route::post('pluAjax', 'ProjectController@getPluAjax');

    Route::post('editplu', 'ProjectController@EditPluAjax');

    Route::get('404page', 'ProjectController@getDashBoard');

    Route::get('pdf','ProjectController@export_pdf');

    Route::post('cabPlu', 'ProjectController@getCabPlu');

    Route::post('searchPlu', 'ProjectController@getSearchPlu');


    Route::post('aktivasimember', 'ProjectController@AktivasiMember');



    Route::get('uploadmargin', 'ProjectController@getUploadMargin');
    Route::post('uploadmastermargin', 'ProjectController@PostUploadMargin');
    Route::get('listmastermargin', 'ProjectController@getListMasterMargin');
    Route::get('admin/mastermargin/datatable', 'ProjectController@getMarginDatatable');

    Route::get('changedep', 'ProjectController@getDepartemen');
    Route::get('changekat', 'ProjectController@getKategori');

    Route::post('addmastermargin', 'ProjectController@PostMasterMargin');
    Route::post('MarginAjax', 'ProjectController@getMarginAjax');

    Route::post('editmargin', 'ProjectController@EditMargin');

    Route::get('cabangongkirdialog', 'ProjectController@getCabangViewAjax');

    Route::post('getstoreofbranch','ProjectController@getStoreOfBranch');
    Route::post('getcashierofstore','ProjectController@getCashierOfStore');

    //sales
    Route::get('chartsales', function () {
        return view('admin/chartofsales');
    });
    Route::post('getchartdata', 'ProjectController@getSalesChart');
    Route::get('laporansales', 'ProjectController@returnSales');
    Route::get('getsalesdatatable','ProjectController@getSalesDatatable');
    Route::get('getsalesdatedatatable','ProjectController@getSalesDateDatatable');
    Route::get('getsalesproductdatatable','ProjectController@getSalesProductDatatable');
    Route::get('getsalesdaydatatable','ProjectController@getSalesDayDatatable');
    Route::get('getsalesbranchdatatable','ProjectController@getSalesBranchDatatable');
    Route::get('getsalesrecapdatatable','ProjectController@getSalesRecapDatatable');
    Route::get('getsalesmonthdatatable','ProjectController@getSalesMonthDatatable');
    Route::get('getsalesbranchdatedatatable','ProjectController@getSalesBranchDateDatatable');
    Route::post('exportformsales','ProjectController@exportSales');

    //pareto
    Route::get('laporanpareto', 'ProjectController@returnPareto');
    Route::get('getparetodatatable','ProjectController@getParetoDatatable');
    Route::post('exportformpareto','ProjectController@exportPareto');

    //pb
    Route::get('laporanpb', 'ProjectController@returnPb');
    Route::get('getpbdatatable','ProjectController@getPbDatatable');
    Route::post('exportformpb','ProjectController@exportPb');

    //promo
    Route::get('laporanpromosi', 'ProjectController@returnPromo');
    Route::get('getpromodatatable','ProjectController@getPromoDatatable');

    //arsip
    Route::get('laporanarsip', 'ProjectController@returnArchive');
    Route::get('getarsipdatatable','ProjectController@getArchiveDatatable');
    Route::post('exportformarsip','ProjectController@exportArchive');

    //stok
    Route::get('laporanstok', 'ProjectController@returnStock');
    Route::get('getstokdatatable','ProjectController@getStockDatatable');
    Route::post('exportformstok','ProjectController@exportStock');

    Route::get('laporanmap', function () {
        return view('admin/laporanmap');
    });

    //version
    Route::get('manageversion', function () {
        return view('admin/manageversion');
    });
    Route::post('devgetaccess', "ProjectController@getDevAccessToken");
    Route::post('devgetallver', "ProjectController@getDevAllVersion");
    Route::post('devupdatever', "ProjectController@devUpdateVersion");
    Route::post('devinsertver', "ProjectController@devInsertVersion");
});


//Route For API

Route::post('gettipetmi', 'ProjectApiController@getMasterTipeTmi');
Route::post('getprodmast', 'ProjectApiController@getMasterPlu');

Route::post('authenticate', 'AuthAPIController@authenticate'); 
Route::get('getuserinfo', 'AuthAPIController@getUserFromToken');

Route::get('getcabang', 'ProjectApiController@getCabangApi');









