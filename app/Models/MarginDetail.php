<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarginDetail extends Model
{
    protected $table = "margin_details";
    protected $fillable = ['margin_id', 'kode_igr'];
    public $timestamps = false;
}
