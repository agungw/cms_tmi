<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterPlu extends Model
{
    protected $table = "master_plu";
    protected $fillable = ['mrg_id', 'kodeplu', 'hrg_jual', 'display', 'frac_tmi', 'unit_tmi', 'qty_min', 'qty_max', 'min_dis','frac_igr', 'unit_igr', 'min_jual', 'barcode', 'tag', 'long_desc'];
    public $timestamps = false;

    public static function getPlu($type = "%")
    {
        $result = MasterPlu::Distinct()
            ->SelectRaw('tipe_tmi.nama as tipetmi, margin_min, margin_max, margin_saran, kat_namakategori, kodeplu, display,frac_tmi, unit_tmi, tipe_tmi.kode_tmi as tipe')
            ->Join('master_margin', 'master_plu.mrg_id', '=', 'master_margin.kode_mrg')
            ->leftJoin('tipe_tmi', 'master_margin.kode_tmi', '=', 'tipe_tmi.kode_tmi')
            ->leftJoin('divisi', 'div_kodedivisi', '=', 'div')
            ->leftJoin('department', function ($join) {
                $join->on('master_margin.dep', '=', 'department.dep_kodedepartement');
                $join->on('divisi.div_kodedivisi', '=', 'department.dep_kodedivisi');
            })
            ->leftJoin('category', function ($join) {
                $join->on('master_margin.kat', '=', 'category.kat_kodekategori');
                $join->on('department.dep_kodedepartement', '=', 'category.kat_kodedepartement');
            })
            ->Where('tipe_tmi.kode_tmi', $type)
            ->get();

        return $result;
    }
    
}
