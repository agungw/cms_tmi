<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterToko extends Model
{
    protected $table = "master_toko";
    public $timestamps = false;
}
