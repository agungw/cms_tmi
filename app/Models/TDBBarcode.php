<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBBarcode extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "barcodes";
    protected $fillable =
    [
        'id',
        'product_id',
        'barcode',
        'created_at',
        'updated_at'
    ];

    public function products()
    {
        return $this->belongsTo('App\Models\TDBProduct','product_id');
    }
}
