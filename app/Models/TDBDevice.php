<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBDevice extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "devices";
    protected $fillable =
    [
        'id',
        'device_id',
        'name',
        'model',
        'sdk',
        'imei',
        'created_at',
        'updated_at'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\TDBUser','device_id');
    }
}
