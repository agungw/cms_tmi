<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBEvent extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "events";
    protected $fillable =
    [
        'id',
        'event_type',
        'created_at',
        'updated_at'
    ];

    public function logs()
    {
        return $this->hasMany('App\Models\TDBLog','event_id');
    }
}
