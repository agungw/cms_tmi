<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBEventStatus extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "event_statuses";
    protected $fillable =
    [
        'id',
        'event_status',
        'created_at',
        'updated_at'
    ];

    public function logs()
    {
        return $this->hasMany('App\Models\TDBLog','event_status_id');
    }
}
