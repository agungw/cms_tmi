<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBMasterPlu extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "master_plus";
    protected $fillable =
    [
        'id',
        'plu',
        'description',
        'created_at',
        'updated_at'
    ];
}
