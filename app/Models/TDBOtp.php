<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBOtp extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "otps";
    protected $fillable =
    [
        'id',
        'user_id',
        'otp',
        'created_at',
        'updated_at'
    ];

    public function users()
    {
        return $this->belongsTo('App\Models\TDBUser', 'user_id');
    }
}
