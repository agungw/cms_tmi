<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBOwnership extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "ownerships";
    protected $fillable =
    [
        'id',
        'ownership',
        'created_at',
        'updated_at'
    ];

    public function userProducts()
    {
        return $this->hasMany('App\Models\TDBUserProduct','ownership_id');
    }
}
