<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBPbStatus extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "pb_statuses";
    protected $fillable =
    [
        'id',
        'title',
        'description',
        'created_at',
        'updated_at'
    ];

    public function pbHeaders()
    {
        return $this->hasMany('App\Models\TDBPbHeader','status_id');
    }
}
