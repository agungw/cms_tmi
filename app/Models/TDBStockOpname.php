<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBStockOpname extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "stock_opnames";
    protected $fillable =
    [
        'id',
        'user_product_id',
        'trx_id',
        'last_stock',
        'current_stock',
        'difference_stock',
        'notes',
        'created_at',
        'updated_at'
    ];

    public function userproducts()
    {
        return $this->belongsTo('App\Models\TDBUserProduct','user_product_id');
    }
}
