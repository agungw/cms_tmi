<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBTag extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "tags";
    protected $fillable =
    [
        'id',
        'tags',
        'can_sell',
        'can_order',
        'created_at',
        'updated_at'
    ];

    public function products()
    {
        return $this->hasMany('App\Models\TDBProduct','tag_id');
    }
}
