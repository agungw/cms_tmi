<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TDBVersion extends Model
{
    protected $connection = 'tmibaru';
    protected $table = "version";
    protected $fillable =
    [
        'id',
        'name',
        'desc',
        'flag_lastest',
        'updated_on',
        'created_at',
        'updated_at'
    ];

    public function logs()
    {
        return $this->hasMany('App\Models\TDBLog','version_id');
    }
}
