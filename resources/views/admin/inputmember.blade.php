@extends('dashboard')
@section('content')
    <style>
        .table { width: 100%; }
        .table-striped tr:nth-child(odd) td,
        .table-striped tr:nth-child(odd) th {
            background-color: #dfe6e9;
        }
    </style>
    <div class="container-fluid" style="margin-top: 100px;padding-left: 200px;padding-right: 30px;">
        <div class="row">


                <div class="col-md-12">
                    <!--Project Activity start-->
                    <section class="panel">
                        <div class="panel-body progress-panel">
                            <div class="row">
                                <div class="col-lg-8 task-progress pull-left">
                                    <h1>Register Member TMI</h1>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <button type="button" id="btn_add_member" class="btn btn-primary btn-lg btn-block flat">Tambah Member Baru</button>
                        </div>

                        <div class="panel-body progress-panel">
                            <div class="row">
                                <div class="col-lg-12 task-progress pull-left">
                                    <h1>List Member TMI</h1>
                                </div>
                            </div>
                        </div>

                        {{--<div class="table table-hover">--}}
                            <table class="datatable table table-striped table-bordered responsive" id="dtTable">
                                <thead>
                                <tr>
                                    <th class="font-14" style="text-align: center;">Email</th>
                                    <th class="font-14" style="text-align: center;">Nama Toko</th>
                                    <th class="font-14" style="text-align: center;">Tipe TMI</th>
                                    <th class="font-14" style="text-align: center;">Cabang</th>
                                    <th class="font-14" style="text-align: center;">Kode Member</th>
                                    <th class="font-14" style="text-align: center;">No.HP</th>
                                    <th class="font-14" style="text-align: center;">Alamat</th>
                                    <th class="font-14" style="text-align: center;">Aksi</th>
                                    {{--<th class="font-14" style="text-align: center;">Status</th>--}}
                                </tr>
                                </thead>
                            </table>
                        {{--</div>--}}

                    </section>
                </div>

            <div class="modal fade-scale" id="ModalAktivasiMember" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                            <h4 class="modal-title">Form Aktivasi</h4>
                        </div>
                        <form class="form-horizontal" method="POST" action="">
                            <input type="text" id="txt_tipeid" hidden>
                            <div class="modal-body">
                                <div class="modal-header">
                                    <h4 class="modal-title">Tipe TMI </h4>
                                    <div class="col-md-12 input-group inputs" style="margin-top: 30px; margin-bottom: 30px;padding-left: 0px;padding-right: 0px;">
                                        <span class="input-group-addon flat" style="min-width: 142px;text-align: left">Pilih Tipe TMI</span>
                                        <select class="js-example-basic-single" name="tipemember" id="tipetmi">
                                            <option value="">-- Pilih Tipe Tmi --</option>
                                            {!! $tipetmi !!}
                                        </select>
                                    </div>
                                    <div class="alert alert-danger flat" role="alert">
                                        <strong>Warning!</strong> Mohon Pastikan telah memilih Tipe Member TMI !
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger flat" id="btn_aktivasi"><i class="icon_key"></i>Aktivasi Member</button>
                                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade-scale" id="EditModalMember" role="dialog">
                <div class="modal-dialog modal-dialog" style="position:absolute;margin-left: -200px;">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Member Tmi </h4>
                        </div>
                        <div class="modal-body">

                            <form class="form-horizontal" method="POST" action="">
                                <input type="text" id="txt_id" hidden>

                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">Email </span>
                                    <input type="text" class="input-sm form-control" id="emailx" type="text" name="email">
                                </div>

                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">Nama Toko</span>
                                    <input type="text" class="input-sm form-control" id="namex" type="text" name="name">
                                </div>

                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">Tipe Tmi</span>
                                    <input type="text" name="tipenametmi" min="1" class="input-sm form-control" id="tipenametmi">
                                </div>

                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">Cabang</span>
                                    <input type="text" name="cabang" min="1" class="input-sm form-control" id="cabangx">
                                </div>
                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">Kode member</span>
                                    <input type="text" name="kodemember" min="1" class="input-sm form-control" id="kodememberx">
                                </div>

                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">No Hp</span>
                                    <input type="text" name="nohp" min="1" class="input-sm form-control" id="nohpx">
                                </div>

                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">Alamat </span>
                                    <textarea class="form-control" rows="5" id='addressx' name="address" type="text"></textarea>
                                    {{--<input type="text" name="address" min="1" class="input-sm form-control" id="addressx">--}}
                                </div>

                                <div class="bs-example">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><i class="glyphicon glyphicon-hand-down"></i><span style="color:red; font-weight: bold"> Mau Ubah Tipe Tmi ?</span></a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body" style="padding-bottom: 0px;">
                                                    <p>Periksa kembali pilihan Anda sebelum tekan Tombol Simpan</p>
                                                </div>

                                                <div class="col-md-12 input-group inputs" style="margin-top: 10px; margin-bottom: 10px;">
                                                    <span class="input-group-addon" style="min-width: 142px;text-align: left">Pilih Tipe TMI</span>
                                                    <select class="js-example-basic-single" name="tipe" id="tipex">
                                                        {!! $tipetmi !!}
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id="btn_savemember">Simpan</button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>

            <div class="modal fade-scale" id="AddMemberTmi" role="dialog">
                <div class="modal-dialog modal-dialog" style="position:absolute;margin-left: -200px;">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Register Member Tmi </h4>
                        </div>
                        {{--<div class="modal-header">--}}
                            {{--<h4 class="modal-title">Register Member TMI </h4>--}}

                            <div class="modal-body">
                                <form class="form-horizontal" method="POST" action="">
                                    <input type="text" id="txt_id" hidden>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Nama Toko TMI <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control flat" id='namatoko' name="namatoko">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Alamat Email <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <input type="email" class="form-control" maxlength="50" id='email' name="email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Password <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <input type="password" class="form-control" maxlength="50" id='password' name="Password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Confirm Password <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <input type="password" class="form-control" maxlength="50" id="passwordconfimation" name="Password_confirmation">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Pilih Cabang TMI <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <select class="js-example-basic-single" name="cabang" id="txt_cab" onChange= "changeMember()">
                                                {!! $branch !!}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Pilih Member IGR <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <select id='member' class="js-example-basic-single" name="member">
                                                {!! $member !!}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Pilih Tipe TMI <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <select class="js-example-basic-single" id='txt_tipe' name="tipemember">
                                                {!! $tipetmi !!}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">No. Handphone  <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control flat" maxlength="50" id='nohape' name="NoHp">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Alamat Penagihan  <b style="color:red">*</b></label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" rows="5" id='alamat' name="Alamat" type="text"></textarea>
                                            {{--<input type="text" class="form-control flat" maxlength="150" id='alamat' name="Alamat">--}}
                                        </div>
                                    </div>


                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id="btn_regnewmember">Tambah</button>
                                    </div>
                                </form>
                            </div>

                    </div>

                </div>
            </div>

    </div>
    </div>
    <script>
        var table = $('#dtTable').dataTable( {
            ajax: '{{url('admin/member/datatable')}}',
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],

            columns: [
                { data: 'email', name: 'email'},
                { data: 'store_name', name: 'store_name'},
                { data: 'tipetmi', name: 'tipetmi'},
                { data: 'cabang', name: 'cabang'},
                { data: 'member_code', name: 'member_code'},
                { data: 'phone_number', name: 'phone_number'},
                { data: 'addressmember', name: 'addressmember'},
//                { data: 'aksi', name: 'aksi'},
                { data: 'status', name: 'status'}
            ]
        });


        $(document).on('click', '#btn_add_member', function(){
            var id = $(this).val();

//            $("#txt_id").val(id);
            $("#AddMemberTmi").modal();

        });

        $(document).ready(function(){
            // Add minus icon for collapse element which is open by default
            $(".collapse.in").each(function(){
                $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
            });
            $(".collapse").on('show.bs.collapse', function(){
                $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
            }).on('hide.bs.collapse', function(){
                $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
            });
        });

        $("#btn_regnewmember").click(function(){
            var id = $("#txt_id").val();
            var namatoko = $("#namatoko").val();
            var email = $("#email").val();
            var password = $("#password").val();
            var passwordconfimation = $("#passwordconfimation").val();
            var txt_cab = $("#txt_cab").val();
            var member = $("#member").val();
            var txt_tipe = $("#txt_tipe").val();
            var nohape = $("#nohape").val();
            var alamat = $("#alamat").val();

            if(namatoko == "" || email == "" || password == "" || passwordconfimation == "" || txt_cab == "" || member == "" || txt_tipe == "" || nohape == "" || alamat == ""){
                alert("Silahkan lengkapi data!");
            }else{
                $.ajax({
                    url: 'registermember',
                    type : 'POST',
                    data : {
                        id : id,
                        namatoko : namatoko,
                        email : email,
                        password : password,
                        passwordconfimation : passwordconfimation,
                        txt_cab :txt_cab,
                        member :member,
                        txt_tipe :txt_tipe,
                        nohape :nohape,
                        alamat :alamat
                    },
                    success:function(msg){
                        $.alert('Data berhasil di tambah', {
                            autoClose: true,
                            closeTime: 5000,
//                        position: ['top-center', [-0.70, 0]],
                            position: ['top-right', [-0.42, 0]],
                            title: false,
                            type: 'info',
                            speed: 'normal'
                        });
                        table.api().ajax.reload();
                        $('#AddMemberTmi').modal('hide');

                    }
                });
            }
//            }
        });


        $(document).on('click', '#btn_edit_member', function(){
            var id = $(this).val();

            $("#txt_id").val(id);
            $("#EditModalMember").modal();

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                url:'memberAjax',
                type : 'POST',
                data : {id : id},
                success:function(msg){

                    document.getElementById("emailx").disabled = true;
                    document.getElementById("namex").disabled = false;
                    document.getElementById("tipex").disabled = false
                    document.getElementById("tipenametmi").disabled = true;
                    document.getElementById("cabangx").disabled = true;
                    document.getElementById("kodememberx").disabled = true;
                    document.getElementById("nohpx").disabled = false;
                    document.getElementById("addressx").disabled = false;

                    $("#emailx").val(msg[0]['email']);
                    $("#namex").val(msg[0]['store_name']);
                    $("#tipex").val(msg[0]['tipetmi']);
                    $("#tipenametmi").val(msg[0]['tipetmi']);
                    $("#cabangx").val(msg[0]['cabang']);
                    $("#kodememberx").val(msg[0]['member_code']);
                    $("#nohpx").val(msg[0]['phone_number']);
                    $("#addressx").val(msg[0]['addressmember']);
                }

            });

        });

        $(document).on('click', '#btn_aktivasi_mm', function(){
            $("#txt_tipeid").val($(this).val());
            $("#ModalAktivasiMember").modal();
        });


        $("#btn_aktivasi").click(function(){
            var tipeid = $("#txt_tipeid").val();
            var tipetmi = $("#tipetmi").val();
            $.ajax({
                url: 'aktivasimember',
                type : 'POST',
                data : {
                    tipeid : tipeid,
                    tipetmi : tipetmi
                },
                success:function(msg){
//                    if(data == '1'){
//                        cartReload();
//                        getCartDetail();
//                    }else{
//                        $('#modal-error').html('<div class="alert alert-danger">' + data + '</div>');
//                    }
                    $.alert('Member Berhasil Di Aktivasi', {
                        autoClose: true,
                        closeTime: 5000,
                        position: ['top-right', [-0.42, 0]],
                        title: false,
                        type: 'danger',
                        speed: 'normal'
                    });
                    table.api().ajax.reload();
                    $('#ModalAktivasiMember').modal('hide');

                }
            });
//            }
        });


        $("#btn_savemember").click(function(){
            var id = $("#txt_id").val();
            var namatoko = $("#namex").val();
            var tipetmi = $("#tipex").val();
            var nohp = $("#nohpx").val();
            var address = $("#addressx").val();

            $.ajax({
                url: 'editmember',
                type : 'POST',
                data : {
                    id : id,
                    namatoko : namatoko,
                    tipetmi : tipetmi,
                    nohp : nohp,
                    address : address

                },
                success:function(msg){
                    $.alert('Data berhasil di update', {
                        autoClose: true,
                        closeTime: 5000,
//                        position: ['top-center', [-0.70, 0]],
                        position: ['top-right', [-0.42, 0]],
                        title: false,
                        type: 'info',
                        speed: 'normal'
                    });
                    table.api().ajax.reload();
                    $('#EditModalMember').modal('hide');

                }
            });
//            }
        });

        function changeMember(){
            var cab = $("#txt_cab").val();
            $.ajax({
                url : 'get_member',
                data : {id_cab : cab},
                success : function(msg){
                    document.getElementById("member").disabled = false;
                    $("#member").html(msg);
                    $("#member").js-example-basic-single('refresh');
                }
            });
        }

    </script>
@endsection
